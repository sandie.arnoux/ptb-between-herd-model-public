
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <vector>

#include "MapShedding.h"
#include "../RepetitionHerdState.h"

class MapInEnvironments {
public:
    // Variables ===========================================================
    int _cleaningindicator1 = 0; // non weaned claf
    int _cleaningindicator2 = 0; // weaned calf

    // All environments=====================================================
    std::vector<double> _vEnvInside_1;
    std::vector<double> _vEnvInside_2;
    std::vector<double> _vEnvInside_3;
    std::vector<double> _vEnvInside_4;
    std::vector<double> _vEnvInside_5;
    std::vector<double> _vEnvGeneral;

    std::vector<double> _vEnvOutside_1;
    std::vector<double> _vEnvOutside_2;
    std::vector<double> _vEnvOutside_3;
    std::vector<double> _vEnvOutside;

    std::vector<double> _vEnvMilk;

    MapInEnvironments();
    // Function to initialize environments from a vector
    void initEnv(std::vector<double> env);
    // Function to update the environments
    void update(int t, const MapShedding& ms, const RepetitionHerdState& rhs, int startGrazing, int endGrazing);
    // Function to know if MAP is still in environments
    bool MapRemaining(int tnow);
    // Function to get environments at a time t
    std::vector<double> getEnvironments(int t);
};

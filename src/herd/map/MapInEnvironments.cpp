
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "MapInEnvironments.h"

MapInEnvironments::MapInEnvironments() {
	Parameters& param = Parameters::getInstance();

    _vEnvInside_1.resize(param._totalTimeSteps,0);
    _vEnvInside_2.resize(param._totalTimeSteps,0);
    _vEnvInside_3.resize(param._totalTimeSteps,0);
    _vEnvInside_4.resize(param._totalTimeSteps,0);
    _vEnvInside_5.resize(param._totalTimeSteps,0);
    _vEnvGeneral.resize(param._totalTimeSteps,0);

    _vEnvOutside_1.resize(param._totalTimeSteps,0);
    _vEnvOutside_2.resize(param._totalTimeSteps,0);
    _vEnvOutside_3.resize(param._totalTimeSteps,0);
    _vEnvOutside.resize(param._totalTimeSteps,0);

    _vEnvMilk.resize(param._totalTimeSteps,0);
}

void MapInEnvironments::initEnv(std::vector<double> env) {
	_vEnvInside_1[0] = env[0];
	_vEnvInside_2[0] = env[1];
	_vEnvInside_3[0] = env[2];
	_vEnvInside_4[0] = env[3];
	_vEnvInside_5[0] = env[4];
	_vEnvOutside_1[0] = env[5];
	_vEnvOutside_2[0] = env[6];
	_vEnvOutside_3[0] = env[7];

    // General environment ----------------------------------------------
    _vEnvGeneral[0] = _vEnvInside_1[0] + _vEnvInside_2[0] + _vEnvInside_3[0] + _vEnvInside_4[0] + _vEnvInside_5[0];

    // Env. ext. -------------------------------------------------------
    _vEnvOutside[0] = _vEnvOutside_1[0] + _vEnvOutside_2[0] + _vEnvOutside_3[0];
}

void MapInEnvironments::update(int t, const MapShedding& ms, const RepetitionHerdState& rhs, int startGrazing, int endGrazing) {
	Parameters& param = Parameters::getInstance();
    // Env. interior and exterior --------------------------------------
    // non weaned calf
    _vEnvInside_1[t] = _vEnvInside_1[t-1] * (1-param._deathRateBactInside) + ms._QtyIt_ns;
    if ( (rhs._vCalvesUnWeaned[t-1]) > 0 ) {
        _cleaningindicator1 = 1;
    }
    if ( (rhs._vCalvesUnWeaned[t-1]) == 0 && (_cleaningindicator1 == 1) ) {
        _vEnvInside_1[t] *= (1-param._cleaningCP);
        _cleaningindicator1 = 0;
    }

    if ((param._weekNumber >= startGrazing) && (param._weekNumber <= endGrazing)) {
        // - in the grazing period
        // weaned calf
        _vEnvInside_2[t] = _vEnvInside_2[t-1] * (1-param._deathRateBactInside) + ms._QtyIt_s1;
        if ( (rhs._vCalvesWeanedIn[t-1]) > 0 ) {
            _cleaningindicator2 = 1;
        }
        if ( (rhs._vCalvesWeanedIn[t-1]) == 0 && (_cleaningindicator2 == 1) ) {
            _vEnvInside_2[t] *= (1-param._cleaningCP);
            _cleaningindicator2 = 0;
        }
        // young heifer
        _vEnvInside_3[t] = _vEnvInside_3[t-1] * (1-param._deathRateBactInside);
        // heifer
        _vEnvInside_4[t] = _vEnvInside_4[t-1] * (1-param._deathRateBactInside);
        // adult
        _vEnvInside_5[t] = _vEnvInside_5[t-1] * (1-param._deathRateBactInside);

        _vEnvOutside_1[t] = _vEnvOutside_1[t-1] * (1-param._deathRateBactOutside) + ms._QtyIt_s2;
        _vEnvOutside_2[t] = _vEnvOutside_2[t-1] * (1-param._deathRateBactOutside) + ms._QtyIt_y;
        _vEnvOutside_3[t] = _vEnvOutside_3[t-1] * (1-param._deathRateBactOutside) + ms._QtyIt_g + ms._QtyIm_g + ms._QtyIh_g;
    } else {
        // - out of the grazing period
        // weaned calf
        _vEnvInside_2[t] = _vEnvInside_2[t-1] * (1-param._deathRateBactInside) + ms._QtyIt_s1 + ms._QtyIt_s2;
        if ( (rhs._vCalvesWeanedOut[t-1]) > 0 ) {
            _cleaningindicator2 = 1;
        }
        if ( (rhs._vCalvesWeanedOut[t-1]) == 0 && (_cleaningindicator2 == 1) ) {
            _vEnvInside_2[t] *= (1-param._cleaningCP);
            _cleaningindicator2 = 0;
        }
        // young heifer
        _vEnvInside_3[t] = _vEnvInside_3[t-1] * (1-param._deathRateBactInside) + ms._QtyIt_y;
        // heifer
        _vEnvInside_4[t] = _vEnvInside_4[t-1] * (1-param._deathRateBactInside) + ms._QtyIt_g + ms._QtyIm_g + ms._QtyIh_g;
        // adult
        _vEnvInside_5[t] = _vEnvInside_5[t-1] * (1-param._deathRateBactInside) + ms._QtyIm_c + ms._QtyIh_c;
 
        _vEnvOutside_1[t] = 0; _vEnvOutside_2[t] = 0; _vEnvOutside_3[t] = 0;
    }

    // General environment ----------------------------------------------
    _vEnvGeneral[t] = _vEnvInside_1[t] + _vEnvInside_2[t] + _vEnvInside_3[t] + _vEnvInside_4[t] + _vEnvInside_5[t];

    // Env. ext. -------------------------------------------------------
    _vEnvOutside[t] = _vEnvOutside_1[t] + _vEnvOutside_2[t] + _vEnvOutside_3[t];

    // Milk ------------------------------------------------------------
    if (param._milkTR == true) {
        if (ms._milkTot > 0) {
            _vEnvMilk[t] = param._qtyMilkDrunk * (ms._QtyMilkIm + ms._QtyMilkIh) / ms._milkTot;
        } else {
            _vEnvMilk[t] = 0;
        }
    }
}

bool MapInEnvironments::MapRemaining(int tnow) {
	if ((_vEnvGeneral[tnow]+_vEnvOutside[tnow])<1) {
		return false;
	} else {
		return true;
	}
}

std::vector<double> MapInEnvironments::getEnvironments(int t) {
	return {_vEnvInside_1[t],_vEnvInside_2[t],_vEnvInside_3[t],
			_vEnvInside_4[t],_vEnvInside_5[t],                 
			_vEnvOutside_1[t],_vEnvOutside_2[t],_vEnvOutside_3[t]};
}

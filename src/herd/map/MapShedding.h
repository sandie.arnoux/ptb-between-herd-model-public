
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once

#include "../../Parameters.h"

class MapShedding {
public:
	MapShedding();
	void reset();
	void update();

    // quantity of bacteria shed in feces
	// Health status It
    double _QtyIt_ns;	// by unweaned
    double _QtyIt_s1;	// by weaned in
    double _QtyIt_s2;	// by weaned out
    double _QtyIt_y;	// by young heifer
    double _QtyIt_g;	// by heifer
	// Health status Im/Ih
    double _QtyIm_g;	// by heifer
    double _QtyIm_c;	// by cow
    double _QtyIh_g;	// by heifer
    double _QtyIh_c;	// by cow

    // Milk production total and per parity
    double _milkTot;
    double _milkTot_P1;
    double _milkTot_P2;
    double _milkTot_P3;
    double _milkTot_P4;
    double _milkTot_P5;
    // Milk production total from R cows and per parity
    double _milkTotR;
    double _milkTotR_P1;
    double _milkTotR_P2;
    double _milkTotR_P3;
    double _milkTotR_P4;
    double _milkTotR_P5;
    // Milk production total from It cows and per parity
    double _milkTotIt;
    double _milkTotIt_P1;
    double _milkTotIt_P2;
    double _milkTotIt_P3;
    double _milkTotIt_P4;
    double _milkTotIt_P5;
    // Milk production total from Il cows and per parity
    double _milkTotIl;
    double _milkTotIl_P1;
    double _milkTotIl_P2;
    double _milkTotIl_P3;
    double _milkTotIl_P4;
    double _milkTotIl_P5;
    // Milk production total from Im cows and per parity
    double _milkTotIm;
    double _milkTotIm_P1;
    double _milkTotIm_P2;
    double _milkTotIm_P3;
    double _milkTotIm_P4;
    double _milkTotIm_P5;
    // Milk production total from Ih cows and per parity
    double _milkTotIh;
    double _milkTotIh_P1;
    double _milkTotIh_P2;
    double _milkTotIh_P3;
    double _milkTotIh_P4;
    double _milkTotIh_P5;
    // Number of bacteria shed in milk by Im and Ih
    double _QtyMilkIm;
    double _QtyMilkIh;
};


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "MapShedding.h"

MapShedding::MapShedding() {
  	_QtyIt_ns = 0;
 	_QtyIt_s1 = 0;
	_QtyIt_s2 = 0;
    _QtyIt_y = 0;
    _QtyIt_g = 0;

    _QtyIm_g = 0;
    _QtyIm_c = 0;
    _QtyIh_g = 0;
    _QtyIh_c = 0;

    _milkTot = 0;
    _milkTot_P1 = 0;
    _milkTot_P2 = 0;
    _milkTot_P3 = 0;
    _milkTot_P4 = 0;
    _milkTot_P5 = 0;

    _milkTotR = 0;
    _milkTotR_P1 = 0;
    _milkTotR_P2 = 0;
    _milkTotR_P3 = 0;
    _milkTotR_P4 = 0;
    _milkTotR_P5 = 0;

    _milkTotIt = 0;
    _milkTotIt_P1 = 0;
    _milkTotIt_P2 = 0;
    _milkTotIt_P3 = 0;
    _milkTotIt_P4 = 0;
    _milkTotIt_P5 = 0;

    _milkTotIl = 0;
    _milkTotIl_P1 = 0;
    _milkTotIl_P2 = 0;
    _milkTotIl_P3 = 0;
    _milkTotIl_P4 = 0;
    _milkTotIl_P5 = 0;

    _milkTotIm = 0;
    _milkTotIm_P1 = 0;
    _milkTotIm_P2 = 0;
    _milkTotIm_P3 = 0;
    _milkTotIm_P4 = 0;
    _milkTotIm_P5 = 0;

    _milkTotIh = 0;
    _milkTotIh_P1 = 0;
    _milkTotIh_P2 = 0;
    _milkTotIh_P3 = 0;
    _milkTotIh_P4 = 0;
    _milkTotIh_P5 = 0;

    _QtyMilkIm = 0;
    _QtyMilkIh = 0;
}

void MapShedding::reset() {
  	_QtyIt_ns = 0;
 	_QtyIt_s1 = 0;
	_QtyIt_s2 = 0;
    _QtyIt_y = 0;
    _QtyIt_g = 0;

    _QtyIm_g = 0;
    _QtyIm_c = 0;
    _QtyIh_g = 0;
    _QtyIh_c = 0;

    _milkTot = 0;
    _milkTot_P1 = 0;
    _milkTot_P2 = 0;
    _milkTot_P3 = 0;
    _milkTot_P4 = 0;
    _milkTot_P5 = 0;

    _milkTotR = 0;
    _milkTotR_P1 = 0;
    _milkTotR_P2 = 0;
    _milkTotR_P3 = 0;
    _milkTotR_P4 = 0;
    _milkTotR_P5 = 0;

    _milkTotIt = 0;
    _milkTotIt_P1 = 0;
    _milkTotIt_P2 = 0;
    _milkTotIt_P3 = 0;
    _milkTotIt_P4 = 0;
    _milkTotIt_P5 = 0;

    _milkTotIl = 0;
    _milkTotIl_P1 = 0;
    _milkTotIl_P2 = 0;
    _milkTotIl_P3 = 0;
    _milkTotIl_P4 = 0;
    _milkTotIl_P5 = 0;

    _milkTotIm = 0;
    _milkTotIm_P1 = 0;
    _milkTotIm_P2 = 0;
    _milkTotIm_P3 = 0;
    _milkTotIm_P4 = 0;
    _milkTotIm_P5 = 0;

    _milkTotIh = 0;
    _milkTotIh_P1 = 0;
    _milkTotIh_P2 = 0;
    _milkTotIh_P3 = 0;
    _milkTotIh_P4 = 0;
    _milkTotIh_P5 = 0;

    _QtyMilkIm = 0;
    _QtyMilkIh = 0;
}

void MapShedding::update() {
	Parameters& param = Parameters::getInstance();
    _QtyIt_g *= param._adultsTogether;
    _QtyIm_g *= param._adultsTogether;
    _QtyIm_c *= param._adultsTogether;
    _QtyIh_g *= param._adultsTogether;
    _QtyIh_c *= param._adultsTogether;

}

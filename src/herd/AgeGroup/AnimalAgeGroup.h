
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <iostream>
#include <map>
#include <memory>

#include "../../Parameters.h"
#include "../../Utils.h"
#include "../Herd.h"
#include "../RepetitionHerdState.h"
#include "../map/MapShedding.h"
#include "../map/MapInEnvironments.h"

class Animal;

class AnimalAgeGroup {
public:
    virtual enumAnimal::AgeGroup getAgeGroup() = 0;
    virtual std::string getAgeGroupStr() = 0;

    virtual void saveHeadcount(int t, RepetitionHerdState& rhs, Animal& iAnimal) = 0;
    virtual int calving(int t, Animal& animal);
    virtual void exitAging(int t, Animal& animal) = 0;
    virtual void updateConsumedInfectedColostrum(float& colostrum, int dam_hs);
    virtual void shedding(MapShedding& ms, int hs, short int& parity) = 0;
    virtual void infection(int t, Animal& animal) = 0;
    virtual void transition(int t, Animal& animal);

    static std::vector<double> getQuantityBacteriaShed(enumAnimal::HealthStatus hs, int number);
};


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Cow.h"
#include "../Animal.h"

void Cow::saveHeadcount(int t, RepetitionHerdState& rhs, Animal& iAnimal) {
	rhs._vCows[t]++;
	if (iAnimal.getHealthStatus() != enumAnimal::S) {
		rhs._vInfectedAdults[t]++;
	}
    switch (iAnimal.getHealthStatus()) { //WARNING: no R represented, so S are counted as R because healthy adults won't be infected...
        case enumAnimal::S: rhs._vRtot[t]++; break;
        case enumAnimal::It: rhs._vIttot[t]++; break;
        case enumAnimal::Il: rhs._vIltot[t]++; break;
        case enumAnimal::Im: rhs._vImtot[t]++; break;
        case enumAnimal::Ih: rhs._vIhtot[t]++; break;
    }
    rhs._vTwoYearsOld[t]++;
    if (iAnimal.getHealthStatus() != enumAnimal::S){
        rhs._vInfectedTwoYearsOld[t]++;
    }
}

int Cow::calving(int t, Animal& animal) {
	Parameters& param = Parameters::getInstance();
	int newborn = 0;
	double infinutero = 0;
	switch (animal.getHealthStatus()) {
		case enumAnimal::S: infinutero = 0; break;
		case enumAnimal::It: infinutero = param._infinuteroIt; break;
		case enumAnimal::Il: infinutero = param._infinuteroIl; break;
		case enumAnimal::Im: infinutero = param._infinuteroIm; break;
		case enumAnimal::Ih: infinutero = param._infinuteroIh; break;
	}
    if (Utils::bernoulli(1 - animal.getHerd()->getDeathRateBirth())) {
        animal.initializeTimeBeforeNextCalving();
        if (Utils::bernoulli(infinutero)) {
            newborn = 1;
        } else {
            newborn = 0;
        }
    } else {
        newborn = -1;
    }
    animal.incrementParity();
	return newborn;
}

void Cow::exitAging(int t, Animal& iAnimal) {
	Parameters& iParams = Parameters::getInstance();
    iAnimal.updateTimeBeforeNextCalving();
    int index = std::min(std::floor((iAnimal.getAge() - iParams._ageFirstCalving) / static_cast<float>(iParams.weeksNbPerYear)), static_cast<float>(iParams.categoriesNbForCows-1));
    double cullingRate = iAnimal.getHerd()->getCullingRates()[index];
    switch (iAnimal.getHealthStatus()) {
        case enumAnimal::S:
            if(!Utils::bernoulli(1 - cullingRate)) {
                iAnimal.setExitReason(enumAnimal::death);
            } break;
        case enumAnimal::It:
            if(!Utils::bernoulli(1 - cullingRate)) {
                iAnimal.setExitReason(enumAnimal::death);
            } break;
        case enumAnimal::Il:
            if(!Utils::bernoulli(1 - cullingRate)) {
                iAnimal.setExitReason(enumAnimal::death);
            } break;
        case enumAnimal::Im:
            if(!Utils::bernoulli((1 - cullingRate) * (1 - iParams._cullingRateIm))) {
                iAnimal.setExitReason(enumAnimal::death);
            } break;
        case enumAnimal::Ih:
            if(!Utils::bernoulli(1 - iAnimal.getCullingProbaIh())) {
                iAnimal.setExitReason(enumAnimal::culling);
                iAnimal.getHerdState()._vExitCowIh[iParams._currentTime]++;
            }
            break;
    }
	if (iAnimal.getExitReason() != enumAnimal::noExit) {
		iAnimal.getHerdState()._vExitCow[iParams._currentTime]++;
	} else {
		iAnimal.incrementAge();
	}
}

void Cow::shedding(MapShedding& ms, int hs, short int& parity) {
	Parameters& param = Parameters::getInstance();
	switch (hs) {
		case enumAnimal::S :
			if (Utils::bernoulli(param._propLactatingCows)) {
				ms._milkTot += param._qtyMilkProd;
				ms._milkTotR += param._qtyMilkProd;
				switch (parity) {
					case 1 : ms._milkTotR_P1 += param._qtyMilkProd; break;
					case 2 : ms._milkTotR_P2 += param._qtyMilkProd; break;
					case 3 : ms._milkTotR_P3 += param._qtyMilkProd; break;
					case 4 : ms._milkTotR_P4 += param._qtyMilkProd; break;
					default : ms._milkTotR_P5 += param._qtyMilkProd; break;
				}
			}
			break;
		case enumAnimal::It:
			if (Utils::bernoulli(param._propLactatingCows)) {
				ms._milkTot += param._qtyMilkProd;
				ms._milkTotIt += param._qtyMilkProd;
				switch (parity) {
					case 1 : ms._milkTotIt_P1 += param._qtyMilkProd; break;
					case 2 : ms._milkTotIt_P2 += param._qtyMilkProd; break;
					case 3 : ms._milkTotIt_P3 += param._qtyMilkProd; break;
					case 4 : ms._milkTotIt_P4 += param._qtyMilkProd; break;
					default : ms._milkTotIt_P5 += param._qtyMilkProd; break;
				}
			}
			break;
		case enumAnimal::Il :
			if(Utils::bernoulli(param._propLactatingCows)) {
				ms._milkTot += param._qtyMilkProdIl;
				ms._milkTotIl += param._qtyMilkProdIl;
				switch (parity) {
					case 1 : ms._milkTotIl_P1 += param._qtyMilkProdIl; break;
					case 2 : ms._milkTotIl_P2 += param._qtyMilkProdIl; break;
					case 3 : ms._milkTotIl_P3 += param._qtyMilkProdIl; break;
					case 4 : ms._milkTotIl_P4 += param._qtyMilkProdIl; break;
					default : ms._milkTotIl_P5 += param._qtyMilkProdIl; break;
				}
			}
			break;
		case enumAnimal::Im :
			ms._QtyIm_c += param._qtyFaecesAd * pow(10, (4+10 * Utils::beta(param._alphaFaecesIm, param._betaFaecesIm)));
			if (Utils::bernoulli(param._propLactatingCows)) {
				ms._milkTot += param._qtyMilkProdIm;
				ms._milkTotIm += param._qtyMilkProdIm;
				switch (parity) {
					case 1 : ms._milkTotIm_P1 += param._qtyMilkProdIm; break;
					case 2 : ms._milkTotIm_P2 += param._qtyMilkProdIm; break;
					case 3 : ms._milkTotIm_P3 += param._qtyMilkProdIm; break;
					case 4 : ms._milkTotIm_P4 += param._qtyMilkProdIm; break;
					default : ms._milkTotIm_P5 += param._qtyMilkProdIm; break;
				}
				ms._QtyMilkIm += param._qtyMilkProdIm * (1 + 1000 * Utils::beta(param._alphaMilkIndirIm, param._betaMilkIndirIm));
				if (Utils::bernoulli(param._propExcreMilkIm)) {
					ms._QtyMilkIm += param._qtyMilkProdIm * (pow(10, 3) * 100 * Utils::beta(param._alphaMilkDirIm, param._betaMilkDirIm));
				}
			}
			break;
		case enumAnimal::Ih :
			ms._QtyIh_c += param._qtyFaecesAd * pow(10, (8+10 * Utils::beta(param._alphaFaecesIh, param._betaFaecesIh)));
			if (Utils::bernoulli(param._propLactatingCows)) {
				ms._milkTot += param._qtyMilkProdIh;
				ms._milkTotIh += param._qtyMilkProdIh;
				switch (parity) {
					case 1 : ms._milkTotIh_P1 += param._qtyMilkProdIh; break;
					case 2 : ms._milkTotIh_P2 += param._qtyMilkProdIh; break;
					case 3 : ms._milkTotIh_P3 += param._qtyMilkProdIh; break;
					case 4 : ms._milkTotIh_P4 += param._qtyMilkProdIh; break;
					default : ms._milkTotIh_P5 += param._qtyMilkProdIh; break;
				}
				ms._QtyMilkIh += param._qtyMilkProdIh * pow(10, (3+10 * Utils::beta(param._alphaMilkIndirIh, param._betaMilkIndirIh)));
				if (Utils::bernoulli(param._propExcreMilkIh)) {
					ms._QtyMilkIh += param._qtyMilkProdIh * (pow(10, 3) * 100 * Utils::beta(param._alphaMilkDirIh, param._betaMilkDirIh));
				}
			}
			break;
	}
}

void Cow::infection(int t, Animal& animal) {
	Parameters& param = Parameters::getInstance();
	if (animal.getHealthStatus() == enumAnimal::S) {
		if (not animal.getHerd()->isGrazingPeriod()) {
			if (Utils::bernoulli((1 - exp( -exp(-param._susceptibilityCoeff*(animal.getAge())) * param._infGeneralEnv * animal.getMapInEnv()._vEnvGeneral[t] / animal.getHerd()->getNumberOfAnimalsInBuilding() / param._infectiousDose)))) {
				animal.setHealthStatus(enumAnimal::It);
				animal.getHerdState()._vIncidenceIt[t]++;
			}
		}
	}
}

void Cow::transition(int t, Animal& animal) {
	int hs = animal.getHealthStatus();
	if (hs != enumAnimal::S and hs != enumAnimal::Ih) {
		Parameters& param = Parameters::getInstance();
		switch(hs) {
			case enumAnimal::It:
				if (Utils::bernoulli(1./param._durShedding)) {
					animal.setHealthStatus(enumAnimal::Il);
					animal.getHerdState()._vIncidenceIl[t]++;
				}
				break;
			case enumAnimal::Il:
				if (Utils::bernoulli(1./param._durIl)) {
					animal.setHealthStatus(enumAnimal::Im);
					animal.getHerdState()._vIncidenceIm[t]++;
				}
				break;
			case enumAnimal::Im:
				if (Utils::bernoulli(1./param._durIm)) {
					animal.setHealthStatus(enumAnimal::Ih);
					animal.getHerdState()._vIncidenceIh[t]++;
				}
				break;
		}
	}
}

enumAnimal::AgeGroup Cow::getAgeGroup() {
	return enumAnimal::cow;
}

std::string Cow::getAgeGroupStr() {
	return std::string("Cow");
}


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "WeanedInCalf.h"
#include "../Animal.h"

void WeanedInCalf::saveHeadcount(int t, RepetitionHerdState& rhs, Animal& iAnimal) {
	rhs._vCalvesWeanedIn[t]++;
	switch (iAnimal.getHealthStatus()) {
		case enumAnimal::S: rhs._vStot[t]++;break;
		case enumAnimal::It: rhs._vIttot[t]++;break;
		case enumAnimal::Il: rhs._vIltot[t]++;break;
		case enumAnimal::Im: rhs._vImtot[t]++;break;
		case enumAnimal::Ih: rhs._vIhtot[t]++;break;
	}
}

void WeanedInCalf::exitAging(int t, Animal& animal){
	Parameters& param = Parameters::getInstance();
	if (Utils::bernoulli(animal.getHerd()->getDeathRates()[animal.getAge()])) {
		animal.setExitReason(enumAnimal::death);
	}
	if (animal.getExitReason() == enumAnimal::noExit) {
		animal.incrementAge();
		if (animal.getAge() >= param._ageGrazing) {
			animal.setShPtrAgegroup(animal.getHerd()->getAnimalAgeGroup(enumAnimal::weanedOutCalf));
		}
	}
}

void WeanedInCalf::shedding(MapShedding& ms, int hs, short int& parity) {
	if (hs == enumAnimal::It) {
		Parameters& param = Parameters::getInstance();
		ms._QtyIt_s1 += param._qtyFaecesCw * pow(10, 4) * 100 * Utils::beta(param._alphaFaecesIt, param._betaFaecesIt);
	}
}

void WeanedInCalf::infection(int t, Animal& animal) {
	Parameters& param = Parameters::getInstance();
	// Infection by Local environments in and out of the grazing period
	if (animal.getHealthStatus() == enumAnimal::S && param._localTR == true) {
		if (animal.getHerd()->isGrazingPeriod()) {
			if (Utils::bernoulli(1 - exp( -exp(-param._susceptibilityCoeff*(animal.getAge())) * param._infLocalEnv * animal.getMapInEnv()._vEnvInside_2[t] / animal.getHerd()->getNumberOfAnimals(enumAnimal::weanedInCalf) / param._infectiousDose))) {
				animal.setHealthStatus(enumAnimal::It);
				animal.getHerdState()._vAgeAtInfection[animal.getAge()]++;
				animal.getHerdState()._vAgeAtInfection_local[animal.getAge()]++;
				animal.getHerdState()._vTransmissionRoutes_local[t]++;
				animal.getHerdState()._vIncidenceIt[t]++;
			}
		// Out of grazing period
		} else {
			if (Utils::bernoulli(1 - exp( -exp(-param._susceptibilityCoeff*(animal.getAge())) * param._infLocalEnv * animal.getMapInEnv()._vEnvInside_2[t] / (animal.getHerd()->getNumberOfAnimals(enumAnimal::weanedInCalf) + animal.getHerd()->getNumberOfAnimals(enumAnimal::weanedOutCalf)) / param._infectiousDose))) {
				animal.setHealthStatus(enumAnimal::It);
				animal.getHerdState()._vAgeAtInfection[animal.getAge()]++;
				animal.getHerdState()._vAgeAtInfection_local[animal.getAge()]++;
				animal.getHerdState()._vTransmissionRoutes_local[t]++;
				animal.getHerdState()._vIncidenceIt[t]++;
			}
		}
	}
	// If no infection by General environments
	if (animal.getHealthStatus() == enumAnimal::S) {
		if (Utils::bernoulli(1 - exp( -exp(-param._susceptibilityCoeff*(animal.getAge())) * (param._infGeneralEnv*animal.getHerd()->getWeanedExpo()) * animal.getMapInEnv()._vEnvGeneral[t] / animal.getHerd()->getNumberOfAnimalsInBuilding() / param._infectiousDose))) {
			animal.setHealthStatus(enumAnimal::It);
			animal.getHerdState()._vAgeAtInfection[animal.getAge()]++;
			animal.getHerdState()._vAgeAtInfection_general[animal.getAge()]++;
			animal.getHerdState()._vTransmissionRoutes_general[t]++;
			animal.getHerdState()._vIncidenceIt[t]++;
		}
	}
}

void WeanedInCalf::transition(int t, Animal& animal) {
	if (animal.getHealthStatus() == enumAnimal::It) {
		Parameters& param = Parameters::getInstance();
		if (Utils::bernoulli(1./param._durShedding)) {
			animal.setHealthStatus(enumAnimal::Il);
			animal.getHerdState()._vIncidenceIl[t]++;
		}
	}
}

enumAnimal::AgeGroup WeanedInCalf::getAgeGroup() {
	return enumAnimal::weanedInCalf;
}

std::string WeanedInCalf::getAgeGroupStr() {
	return std::string("Weaned In calf");
}


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "NewBorn.h"
#include "../Animal.h"

void NewBorn::saveHeadcount(int t, RepetitionHerdState& rhs, Animal& iAnimal) {
	rhs._vCalvesNewBorn[t]++;
	switch (iAnimal.getHealthStatus()) {
		case enumAnimal::S: rhs._vStot[t]++;break;
		case enumAnimal::It: rhs._vIttot[t]++;break;
		case enumAnimal::Il: rhs._vIltot[t]++;break;
		case enumAnimal::Im: rhs._vImtot[t]++;break;
		case enumAnimal::Ih: rhs._vIhtot[t]++;break;
	}
}

void NewBorn::exitAging(int t, Animal& animal) {
	Parameters& param = Parameters::getInstance();
	if (Utils::bernoulli(animal.getHerd()->getDeathRates()[0])) {
		animal.setExitReason(enumAnimal::death);
	} else {
		animal.incrementAge();
		animal.setShPtrAgegroup(animal.getHerd()->getAnimalAgeGroup(enumAnimal::unweanedCalf));
	}
}

void NewBorn::updateConsumedInfectedColostrum(float& colostrum, int dam_hs) {
	Parameters& param = Parameters::getInstance();
	if (dam_hs == enumAnimal::Im) {
		if (Utils::bernoulli(param._propExcreColoIm)) {
			colostrum = param._qtyColoDrunk * ( (100 * pow(10, 3) * Utils::beta(param._alphaColoDirIm, param._betaColoDirIm)) + (1 + 1000 * Utils::beta(param._alphaColoIndirIm, param._betaColoIndirIm)) );
		}
	} else if (dam_hs == enumAnimal::Ih) {
		if (Utils::bernoulli(param._propExcreColoIh)) {
			colostrum = param._qtyColoDrunk * ( (100 * pow(10, 3) * Utils::beta(param._alphaColoDirIh, param._betaColoDirIh)) + pow(10, (3+10 * Utils::beta(param._alphaColoIndirIh, param._betaColoIndirIh))) );
		}
	}
}

void NewBorn::shedding(MapShedding& ms, int hs, short int& parity) {
	if (hs == enumAnimal::It) {
		Parameters& param = Parameters::getInstance();
		ms._QtyIt_ns += param._qtyFaecesCnw * pow(10, 4) * 100 * Utils::beta(param._alphaFaecesIt, param._betaFaecesIt);
	}
}

void NewBorn::infection(int t, Animal& animal) {
	Parameters& param = Parameters::getInstance();
	// Infection by colostrum
	if (animal.getHealthStatus() == enumAnimal::S && param._colostrumTR == true) {
		if (Utils::bernoulli(1 - exp( -param._infIngestion * animal.getConsumedInfectedColostrum() / param._infectiousDose))) {
			animal.setHealthStatus(enumAnimal::It);
			animal.getHerdState()._vAgeAtInfection[animal.getAge()]++;
			animal.getHerdState()._vAgeAtInfection_colostrum[animal.getAge()]++;
			animal.getHerdState()._vTransmissionRoutes_colostrum[t]++;
			animal.getHerdState()._vIncidenceIt[t]++;
		}
	}
	// Infection by milk
	if (animal.getHealthStatus() == enumAnimal::S && param._milkTR == true) {
		if(Utils::bernoulli(1 - exp( -param._infIngestion * (4./7.) * animal.getMapInEnv()._vEnvMilk[t] / param._infectiousDose))) {
			animal.setHealthStatus(enumAnimal::It);
			animal.getHerdState()._vAgeAtInfection[animal.getAge()]++;
			animal.getHerdState()._vAgeAtInfection_milk[animal.getAge()]++;
			animal.getHerdState()._vTransmissionRoutes_milk[t]++;
			animal.getHerdState()._vIncidenceIt[t]++;
		}
	}
	// Infection by Local environments
	if (animal.getHealthStatus() == enumAnimal::S && param._localTR == true) {
		if (Utils::bernoulli(1 - exp( -exp(-param._susceptibilityCoeff*(animal.getAge())) * param._infLocalEnv * animal.getMapInEnv()._vEnvInside_1[t] / (animal.getHerd()->getNumberOfAnimals(enumAnimal::newBorn) + animal.getHerd()->getNumberOfAnimals(enumAnimal::unweanedCalf)) / param._infectiousDose))) {
			animal.setHealthStatus(enumAnimal::It);
			animal.getHerdState()._vAgeAtInfection[animal.getAge()]++;
			animal.getHerdState()._vAgeAtInfection_local[animal.getAge()]++;
			animal.getHerdState()._vTransmissionRoutes_local[t]++;
			animal.getHerdState()._vIncidenceIt[t]++;
		}
	}
	// Infection by General environments
	if (animal.getHealthStatus() == enumAnimal::S) {
		if (Utils::bernoulli(1 - exp( -exp(-param._susceptibilityCoeff*(animal.getAge())) * (param._infGeneralEnv*animal.getHerd()->getUnweanedExpo()) * animal.getMapInEnv()._vEnvGeneral[t] / animal.getHerd()->getNumberOfAnimalsInBuilding() / param._infectiousDose))) {
			animal.setHealthStatus(enumAnimal::It);
			animal.getHerdState()._vAgeAtInfection[animal.getAge()]++;
			animal.getHerdState()._vAgeAtInfection_general[animal.getAge()]++;
			animal.getHerdState()._vTransmissionRoutes_general[t]++;
			animal.getHerdState()._vIncidenceIt[t]++;
		}
	}
}

enumAnimal::AgeGroup NewBorn::getAgeGroup() {
	return enumAnimal::newBorn;
}

std::string NewBorn::getAgeGroupStr() {
	return std::string("New born");
}


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "AnimalAgeGroup.h"
#include "../Animal.h"

int AnimalAgeGroup::calving(int t, Animal& animal) {return -1;}
void AnimalAgeGroup::updateConsumedInfectedColostrum(float& colostrum, int dam_hs) {}
void AnimalAgeGroup::transition(int t, Animal& animal) {}

// Function to get the quantity distribution of bacteria shed by kg of feces per week
std::vector<double> AnimalAgeGroup::getQuantityBacteriaShed(enumAnimal::HealthStatus hs, int number) {
	Parameters& param = Parameters::getInstance();
	// initialize vector of shed bacteria
	std::vector<double> vQuantityBacteria;
	vQuantityBacteria.resize(number,0.0);
	// case of health status
	switch (hs) {
		case enumAnimal::S : break;
		case enumAnimal::It :
			for (int i=0; i < number; i++) {
				vQuantityBacteria[i] = pow(10, 4) * 100 * Utils::beta(param._alphaFaecesIt, param._betaFaecesIt);
			}
			break;
		case enumAnimal::Il : break;
		case enumAnimal::Im :
			for (int i=0; i < number; i++) {
				vQuantityBacteria[i] = pow(10, (4+10 * Utils::beta(param._alphaFaecesIm, param._betaFaecesIm)));
			}
			break;
		case enumAnimal::Ih :
			for (int i=0; i < number; i++) {
				vQuantityBacteria[i] = pow(10, (8+10 * Utils::beta(param._alphaFaecesIh, param._betaFaecesIh)));
			}
			break;
	}
	return vQuantityBacteria;
}

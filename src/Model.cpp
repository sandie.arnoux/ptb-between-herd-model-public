
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Model.h"

void Model::loadDemography(){
    Parameters& iParams = Parameters::getInstance();
    std::string line;
    std::string id;
    std::string fileName = iParams.dataPath + iParams.vDataFiles[2];
    std::ifstream fHC(fileName.c_str());
    if (not fHC.is_open()) throw std::runtime_error("ERROR: can't open "+ fileName +" file.");
    while (std::getline(fHC, line)){
        id = Utils::getFirstWord(line, '\t');
        vHerdIds.push_back(id);
        mvHeadcountPerHerdPerAgeAtStart.emplace(id, Utils::splitWithoutFirstItem(line, '\t'));
    }
    vHerdIds.shrink_to_fit();
    fHC.close();
    fileName = iParams.dataPath + iParams.vDataFiles[0];
    std::ifstream fBirths(fileName.c_str());
    if (not fBirths.is_open()) throw std::runtime_error("ERROR: can't open "+ fileName +" file.");
    while (std::getline(fBirths, line)){
        id = Utils::getFirstWord(line, '\t');
        mvBirthsNbPerHerdPerStep.emplace(id, Utils::splitWithoutFirstItem(line, '\t'));
    }
    fBirths.close();
    fileName = iParams.dataPath + iParams.vDataFiles[1];
    std::ifstream fCullingRates(fileName.c_str());
    if (not fCullingRates.is_open()) throw std::runtime_error("ERROR: can't open "+ fileName +" file.");
    while (std::getline(fCullingRates, line)){
        id = Utils::getFirstWord(line, '\t');
        mvCullingRatesPerHerdPerYearPerGroup.emplace(id, Utils::splitWithoutFirstItem_float(line, '\t'));
    }
    fCullingRates.close();
}

void Model::loadMovements(){
    Parameters& iParams = Parameters::getInstance();
    std::string movesFileName = iParams.dataPath + iParams.vDataFiles[3];
    std::ifstream fMovements(movesFileName.c_str());
    if (not fMovements.is_open()) throw std::runtime_error("ERROR: can't open "+ movesFileName +" file.");
    vmvMovements.resize(iParams.yearsNb * iParams.weeksNbPerYear +1); //+1 because in input file, date is from 1 to 469 (instead of 9*52=468)
    std::string line;
    std::vector<std::string> vOneMove(4);
    std::string sourceId;
    std::string destId;
    int moveDate;
    int ageAtMove;
    int breed;
    while (std::getline(fMovements, line)){
        vOneMove = Utils::split(line, '\t');
        sourceId = vOneMove[0];
        destId = vOneMove[1];
        moveDate = std::stoi(vOneMove[2]) - 1; //-1 to fit with iterators (they start at 0, but data start at 1)
        ageAtMove = std::stoi(vOneMove[3]) - 1; //-1, because age from 1 to 135 in data, but rates vector from 0 to 134
        breed = std::stoi(vOneMove[5]);
        std::vector<MovementUnitFromData> v;
        vmvMovements[moveDate].emplace(std::pair<std::string, std::vector<MovementUnitFromData>>(sourceId, v));
        vmvMovements[moveDate][sourceId].emplace_back(MovementUnitFromData(destId, ageAtMove, breed));
    }
    fMovements.close();
}

void Model::loadHerdsToRewire(){
    Parameters& iParams = Parameters::getInstance();
    std::string line;
    std::string fileName = iParams.herdsToRewireFile;
    std::ifstream f(fileName.c_str());
    if (not f.is_open()) throw std::runtime_error("ERROR: can't open "+ fileName +" file.");
    while (std::getline(f, line)){
        vHerdIdsToRewire.push_back(line);
    }
    vHerdIdsToRewire.resize(std::round(vHerdIdsToRewire.size() * iParams.propOfHerdsToRewire));
    f.close();
}

void Model::loadSelectedHerds(){
    std::string line;
    std::string fileName = Parameters::getInstance().selectedHerdsFile;
    std::ifstream f(fileName.c_str());
    if (not f.is_open()) throw std::runtime_error("ERROR: can't open "+ fileName +" file.");
    while (std::getline(f, line)){
        vSelectedHerdIds.push_back(line);
    }
    vSelectedHerdIds.shrink_to_fit();
    f.close();
}

void Model::loadInitialInfection(){
    Parameters& iParams = Parameters::getInstance();
    std::string line;
    std::string id;
    std::string fileName = iParams.initInFile;
    std::ifstream f(fileName.c_str());
    if (not f.is_open()) throw std::runtime_error("ERROR: can't open "+ fileName +" file.");
    while (std::getline(f, line)){
        id = Utils::getFirstWord(line, '\t');
        vHerdIdsToInfect.push_back(id);
        mvHerdIdToInfectedNbAndPrevAndRepet.emplace(id, Utils::splitWithoutFirstItem(line, '\t'));
    }
    vHerdIdsToInfect.shrink_to_fit();
    f.close();
}

void Model::loadInitialPrevalence(){
    Parameters& iParams = Parameters::getInstance();
    int prev;
    std::string fileName = iParams.initPrevFile;
    std::ifstream f(fileName.c_str());
    if (not f.is_open()) throw std::runtime_error("ERROR: can't open "+ fileName +" file.");
    while (f >> prev){
        vInitPrev.push_back(prev);
    }
    vInitPrev.shrink_to_fit();
    f.close();
}

void Model::loadInitPrevMatrix(){
    Parameters& iParams = Parameters::getInstance();
    std::string initFileName = iParams.dataPath + iParams.initCondFile;
    std::ifstream f(initFileName.c_str());
    if (not f.is_open()) throw std::runtime_error("ERROR: can't open "+ initFileName +" file.");
    std::vector<double> vBufferHealthStates_avg(5, 0.);
    std::vector<double> vBufferdistributionSR_avg(135, 0.);
    std::vector<double> vBufferdistributionT_avg(135, 0.);
    std::vector<double> vBufferdistributionL_avg(135, 0.);
    std::vector<double> vBufferdistributionIs_avg(135, 0.);
    std::vector<double> vBufferdistributionIc_avg(135, 0.);
    std::vector<double> vBufferEnvironments_avg(12, 0.);
    std::vector<std::vector<double>> vvBuffer_avg;
    vvBuffer_avg.emplace_back(vBufferHealthStates_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionSR_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionT_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionL_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionIs_avg);
    vvBuffer_avg.emplace_back(vBufferdistributionIc_avg);
    vvBuffer_avg.emplace_back(vBufferEnvironments_avg);
    for (auto prev = 1; prev <= iParams.maxInitPrev; prev++) {
        mvvPrevToMeanNbPerStatePerAge[prev] = vvBuffer_avg;
    }
    std::string line;
    std::getline(f, line);
    while (std::getline(f, line)){
        std::stringstream ss(line);
        std::vector<double> vBuffer;
        std::string temp;
        while(std::getline(ss, temp, ',')) {
            vBuffer.emplace_back(std::stod(temp));
        }
        std::vector<double> vBufferHealthStates(vBuffer.begin(), vBuffer.begin()+5); assert(vBufferHealthStates.size() == 5);
        std::vector<double> vBufferdistributionSR(vBuffer.begin()+5, vBuffer.begin()+5+135); assert(vBufferdistributionSR.size() == 135);
        std::vector<double> vBufferdistributionT(vBuffer.begin()+5+135, vBuffer.begin()+5+135*2); assert(vBufferdistributionT.size() == 135);
        std::vector<double> vBufferdistributionL(vBuffer.begin()+5+135*2, vBuffer.begin()+5+135*3); assert(vBufferdistributionL.size() == 135);
        std::vector<double> vBufferdistributionIs(vBuffer.begin()+5+135*3, vBuffer.begin()+5+135*4); assert(vBufferdistributionIs.size() == 135);
        std::vector<double> vBufferdistributionIc(vBuffer.begin()+5+135*4, vBuffer.begin()+5+135*5); assert(vBufferdistributionIc.size() == 135);
        std::vector<double> vBufferEnvironments(vBuffer.begin()+5+135*5, vBuffer.end()); assert(vBufferEnvironments.size() == 12);
        std::vector<std::vector<double>> vvBuffer;
        vvBuffer.emplace_back(vBufferHealthStates);
        vvBuffer.emplace_back(vBufferdistributionSR);
        vvBuffer.emplace_back(vBufferdistributionT);
        vvBuffer.emplace_back(vBufferdistributionL);
        vvBuffer.emplace_back(vBufferdistributionIs);
        vvBuffer.emplace_back(vBufferdistributionIc);
        vvBuffer.emplace_back(vBufferEnvironments);
        int prev = std::round((1.0 - vBufferHealthStates[0]) * 100);
        if (prev > iParams.maxInitPrev){
            break;
        }
        mvvvPrevToRepetOfNbPerStatePerAge[prev].emplace_back(vvBuffer);
        Utils::accumulateInFirstVector(mvvPrevToMeanNbPerStatePerAge[prev][0], vBufferHealthStates);
        Utils::accumulateInFirstVector(mvvPrevToMeanNbPerStatePerAge[prev][1], vBufferdistributionSR);
        Utils::accumulateInFirstVector(mvvPrevToMeanNbPerStatePerAge[prev][2], vBufferdistributionT);
        Utils::accumulateInFirstVector(mvvPrevToMeanNbPerStatePerAge[prev][3], vBufferdistributionL);
        Utils::accumulateInFirstVector(mvvPrevToMeanNbPerStatePerAge[prev][4], vBufferdistributionIs);
        Utils::accumulateInFirstVector(mvvPrevToMeanNbPerStatePerAge[prev][5], vBufferdistributionIc);
        Utils::accumulateInFirstVector(mvvPrevToMeanNbPerStatePerAge[prev][6], vBufferEnvironments);
    }
    f.close();
    for (auto prev = 1; prev <= iParams.maxInitPrev; prev++) {
        double repetitionsNb = static_cast<double>(mvvvPrevToRepetOfNbPerStatePerAge[prev].size());
        Utils::divideInPlace(mvvPrevToMeanNbPerStatePerAge[prev][0], repetitionsNb);
        Utils::divideInPlace(mvvPrevToMeanNbPerStatePerAge[prev][1], repetitionsNb);
        Utils::divideInPlace(mvvPrevToMeanNbPerStatePerAge[prev][2], repetitionsNb);
        Utils::divideInPlace(mvvPrevToMeanNbPerStatePerAge[prev][3], repetitionsNb);
        Utils::divideInPlace(mvvPrevToMeanNbPerStatePerAge[prev][4], repetitionsNb);
        Utils::divideInPlace(mvvPrevToMeanNbPerStatePerAge[prev][5], repetitionsNb);
        Utils::divideInPlace(mvvPrevToMeanNbPerStatePerAge[prev][6], repetitionsNb);
    }
}

void Model::initializeHerds(){
    Parameters& iParams = Parameters::getInstance();
    if (iParams.doLoadingOfInitialInfection){
        std::cout << vHerdIdsToInfect.size() << " herds will be infected as in " << iParams.initInFile << std::endl;
    } else {
        int nbHerdsToInfect = static_cast<int>(iParams.propOfHerdsToInfect * vHerdIds.size());
        std::cout << nbHerdsToInfect << " herds will be infected" << std::endl;
        std::vector<std::string> vCandidateHerdsForInfection = vHerdIds;
        if (iParams.useSelectedHerds and vHerdIds.size() - nbHerdsToInfect < vSelectedHerdIds.size()){
            throw std::runtime_error("Don't know what to do yet...");
        } else if (iParams.useSelectedHerds){
            vCandidateHerdsForInfection.erase(std::remove_if(std::begin(vCandidateHerdsForInfection), std::end(vCandidateHerdsForInfection), 
                                                            [&](std::string id){return std::find(std::begin(vSelectedHerdIds), std::end(vSelectedHerdIds), id) != std::end(vSelectedHerdIds);}), 
                                              std::end(vCandidateHerdsForInfection));
        }
        if (vCandidateHerdsForInfection.size() == nbHerdsToInfect){
            vHerdIdsToInfect = vCandidateHerdsForInfection;
        } else if (vCandidateHerdsForInfection.size() < nbHerdsToInfect){
            throw std::runtime_error("ERROR: "+ std::to_string(vHerdIds.size()) +" x "+ std::to_string(iParams.propOfHerdsToInfect) +" = "+ std::to_string(nbHerdsToInfect) +" herds to infect, so "+ std::to_string(vHerdIds.size()) +" - "+ std::to_string(nbHerdsToInfect) +" = "+ std::to_string(vHerdIds.size() - nbHerdsToInfect) +" won't be infected, but from -u option, "+ std::to_string(vSelectedHerdIds.size()) +" herds can't be infected...");
        } else {
            vHerdIdsToInfect = Utils::randomSamplingWithoutReplacement(vCandidateHerdsForInfection, nbHerdsToInfect);
        }
    }
    std::vector<int> vHeadcountPerAge;
    std::map<std::string, std::vector<int>>::iterator itInitialInfectionMap;
    for (auto& it : mvHeadcountPerHerdPerAgeAtStart){
        auto herdId = it.first;
        auto pHerd = std::make_shared<Herd>(herdId);
        vHeadcountPerAge = it.second;
        std::vector<std::tuple<int, int>> vtAgeAndStateOfInfectedAnimals;
        if (std::find(vHerdIdsToInfect.begin(), vHerdIdsToInfect.end(), herdId) != vHerdIdsToInfect.end()){
            if (iParams.doLoadingOfInitialInfection){
                vtAgeAndStateOfInfectedAnimals = pHerd->initializeInfection(mvvvPrevToRepetOfNbPerStatePerAge, mvvPrevToMeanNbPerStatePerAge, vHeadcountPerAge, mvHerdIdToInfectedNbAndPrevAndRepet.at(herdId));
            } else {
                vtAgeAndStateOfInfectedAnimals = pHerd->initializeInfection(mvvvPrevToRepetOfNbPerStatePerAge, mvvPrevToMeanNbPerStatePerAge, vHeadcountPerAge);
            }
        }
        pHerd->initializeAnimals(vHeadcountPerAge, vtAgeAndStateOfInfectedAnimals);
        pHerd->linkOffspringsToDam(enumAnimal::unweanedCalf);
        pHerd->initializeRates(mvBirthsNbPerHerdPerStep.at(herdId), mvCullingRatesPerHerdPerYearPerGroup.at(herdId));
        if (iParams.useSelectedHerdsToRewire and (std::find(vHerdIdsToRewire.begin(), vHerdIdsToRewire.end(), herdId) != vHerdIdsToRewire.end())){
            pHerd->setUnweanedExpo(iParams.calfExpoForSelectedHerds);
            pHerd->setWeanedExpo(iParams.calfExpoForSelectedHerds);
        }
        mpHerds.emplace(herdId, pHerd);
    }
}

void Model::initializeHerdsAccordingToStatus(){
    Parameters& iParams = Parameters::getInstance();
    int prev;
    int minPrev;
    int maxPrev;
    int nbHerdsToInfect;
    std::vector<int> vHeadcountPerAge;
    std::vector<std::string> vRemainingHerds = vHerdIds;
    for (auto i = 0; i < iParams.vHerdPropPerStatus.size(); i++){
        minPrev = (i == 0) ? 0 : static_cast<int>(iParams.vShedPropThres[i-1] * 100);
        maxPrev = static_cast<int>(iParams.vShedPropThres[i] * 100) - 1;
        maxPrev = (maxPrev > iParams.maxInitPrev) ? iParams.maxInitPrev : maxPrev;
        int nbHerdsToInfect = static_cast<int>(iParams.vHerdPropPerStatus[i] * vHerdIds.size());
        std::cout << nbHerdsToInfect << " herds will have prev < " << iParams.vShedPropThres[i] << std::endl;
        if (nbHerdsToInfect == 0){
            continue;
        }
        std::vector<std::string> vHerdsToInfect;
        if (nbHerdsToInfect < vRemainingHerds.size()){
            vHerdsToInfect = Utils::randomSamplingWithoutReplacement(vRemainingHerds, nbHerdsToInfect);
        } else {
            vHerdsToInfect = vRemainingHerds;
        }
        for (auto id : vHerdsToInfect){
            auto pHerd = std::make_shared<Herd>(id);
            vHeadcountPerAge = mvHeadcountPerHerdPerAgeAtStart[id];
            prev = Utils::uniform(minPrev, maxPrev);
            std::vector<std::tuple<int, int>> vtAgeAndStateOfInfectedAnimals;
            if (prev > 0){
                vtAgeAndStateOfInfectedAnimals = pHerd->initializeInfection(prev, mvvvPrevToRepetOfNbPerStatePerAge, mvvPrevToMeanNbPerStatePerAge, vHeadcountPerAge);
            }
            pHerd->initializeAnimals(vHeadcountPerAge, vtAgeAndStateOfInfectedAnimals);
            pHerd->linkOffspringsToDam(enumAnimal::unweanedCalf);
            pHerd->initializeRates(mvBirthsNbPerHerdPerStep.at(id), mvCullingRatesPerHerdPerYearPerGroup.at(id));
            mpHerds.emplace(id, pHerd);
        }
        for (auto id : vHerdsToInfect){
            vRemainingHerds.erase(std::remove(std::begin(vRemainingHerds), std::end(vRemainingHerds), id), std::end(vRemainingHerds));
        }
    }
    std::cout << "At least " << vRemainingHerds.size() << " herds will have prev == 0 " << std::endl;
    for (auto id : vRemainingHerds){
        auto pHerd = std::make_shared<Herd>(id);
        vHeadcountPerAge = mvHeadcountPerHerdPerAgeAtStart[id];
        std::vector<std::tuple<int, int>> vtAgeAndStateOfInfectedAnimals;
        pHerd->initializeAnimals(vHeadcountPerAge, vtAgeAndStateOfInfectedAnimals);
        pHerd->linkOffspringsToDam(enumAnimal::unweanedCalf);
        pHerd->initializeRates(mvBirthsNbPerHerdPerStep.at(id), mvCullingRatesPerHerdPerYearPerGroup.at(id));
        mpHerds.emplace(id, pHerd);
    }
}

void Model::initializeHerdsAccordingToPrev(){
    Parameters& iParams = Parameters::getInstance();
    int prev;
    std::vector<int> vHeadcountPerAge;
    std::vector<std::string> vRemainingHerds = vHerdIds;
    for (auto i = 0; i < vHerdIds.size(); ++i){
        auto id = vHerdIds.at(i);
        auto prev = vInitPrev.at(i);
        auto pHerd = std::make_shared<Herd>(id);
        vHeadcountPerAge = mvHeadcountPerHerdPerAgeAtStart[id];
        std::vector<std::tuple<int, int>> vtAgeAndStateOfInfectedAnimals;
        if (prev > 0){
            vtAgeAndStateOfInfectedAnimals = pHerd->initializeInfection(prev, mvvvPrevToRepetOfNbPerStatePerAge, mvvPrevToMeanNbPerStatePerAge, vHeadcountPerAge);
        }
        pHerd->initializeAnimals(vHeadcountPerAge, vtAgeAndStateOfInfectedAnimals);
        pHerd->linkOffspringsToDam(enumAnimal::unweanedCalf);
        pHerd->initializeRates(mvBirthsNbPerHerdPerStep.at(id), mvCullingRatesPerHerdPerYearPerGroup.at(id));
        mpHerds.emplace(id, pHerd);
    }
}

void Model::sendMovesFromOutsideMetapop(int stepNum, std::map<std::string, std::vector<MovementUnit>>& mvPendingMovesFromOutside){
    Parameters& iParams = Parameters::getInstance();
    Metapop& iMetapop = Metapop::getInstance();
    for (auto& iMovementUnitFromData : vmvMovements[stepNum][iParams.outsideMetapop]){
        int age = iMovementUnitFromData.age;
        int parity = 0;
        int rewiringType = eNotRewired;
        if (age >= iParams._ageFirstCalving){
            parity = age - iParams._ageFirstCalving +1;
            age = (age - iParams._ageFirstCalving) * iParams.weeksNbPerYear + iParams._ageFirstCalving;
        }
        std::shared_ptr<Animal> pAnimal = std::make_shared<Animal>(nullptr, age, enumAnimal::S, parity);
        if (iParams.isRewiringAllowed and iParams.doRewiringForOutside and iParams.doRewiringForBuyers and (std::find(vHerdIdsToRewire.begin(), vHerdIdsToRewire.end(), iMovementUnitFromData.destinationId) != vHerdIdsToRewire.end())){
            rewiringType = eHealthyAnimal;
        } else {
            std::vector<int> vIndivNbForEachState;
            if (iParams.isRewiringAllowed and iParams.doRewiringForOutside and not iParams.doRewiringForBuyers and not iParams.doRewiringForSellers){
                auto status = mpHerds.at(iMovementUnitFromData.destinationId)->getStatus();
                if (iParams.useHerdHistory){
                    if (status == eA){
                        vIndivNbForEachState = Utils::multinomial(1, iMetapop.getHealthStateProbas(pAnimal->getGroupOfAge(), status));
                        rewiringType = eRewiredOutside;
                    } else {
                        vIndivNbForEachState = Utils::multinomial(1, iMetapop.getHealthStateProbas(pAnimal->getGroupOfAge()));
                    }
                } else {
                    vIndivNbForEachState = Utils::multinomial(1, iMetapop.getHealthStateProbas(pAnimal->getGroupOfAge(), status));
                    rewiringType = eRewiredOutside;
                }
            } else {
                vIndivNbForEachState = Utils::multinomial(1, iMetapop.getHealthStateProbas(pAnimal->getGroupOfAge()));
            }
            auto it = std::find(vIndivNbForEachState.begin(), vIndivNbForEachState.end(), 1);
            if (it != vIndivNbForEachState.end()){
                pAnimal->setHealthStatus(std::distance(vIndivNbForEachState.begin(), it));
            } else {
                std::cout << "No age group found in metapop corresponding to the animal to move..." << std::endl;
            }
        }
        mvOutPendingMoves[iMovementUnitFromData.destinationId].emplace_back(MovementUnit(iParams.outsideMetapop, -1, iMovementUnitFromData.breed, pAnimal, rewiringType));
    }
    for (auto& itMap : mvPendingMovesFromOutside){
        if (iParams.isRewiringAllowed and iParams.doRewiringForbiddenMoves and iParams.doRewiringForBuyers and (std::find(vHerdIdsToRewire.begin(), vHerdIdsToRewire.end(), itMap.first) != vHerdIdsToRewire.end())){
            for (auto& iMovementUnit : itMap.second){
                iMovementUnit.pAnimal->setHealthStatus(enumAnimal::S);
                iMovementUnit.rewiringType = eHealthyAnimal;
                mvOutPendingMoves[itMap.first].emplace_back(iMovementUnit);
            }
        } else {
            auto status = mpHerds.at(itMap.first)->getStatus();
            for (auto& iMovementUnit : itMap.second){
                std::vector<int> vIndivNbForEachState;
                if (iParams.isRewiringAllowed and iParams.doRewiringForbiddenMoves and not iParams.doRewiringForBuyers and not iParams.doRewiringForSellers){
                    vIndivNbForEachState = Utils::multinomial(1, iMetapop.getHealthStateProbas(iMovementUnit.pAnimal->getGroupOfAge(), status));
                    if (iMovementUnit.rewiringType == eNA){
                        iMovementUnit.rewiringType = eRewiredNA;
                    } else if (iMovementUnit.rewiringType == eBroken){
                        iMovementUnit.rewiringType = eRewiredBroken;
                    } else {
                        iMovementUnit.rewiringType = eRewiredOutside;
                    }
                } else {
                    vIndivNbForEachState = Utils::multinomial(1, iMetapop.getHealthStateProbas(iMovementUnit.pAnimal->getGroupOfAge()));
                }
                auto it = std::find(vIndivNbForEachState.begin(), vIndivNbForEachState.end(), 1);
                if (it != vIndivNbForEachState.end()){
                    iMovementUnit.pAnimal->setHealthStatus(std::distance(vIndivNbForEachState.begin(), it));
                } else {
                    std::cout << "No age group found in metapop corresponding to the animal to move..." << std::endl;
                }
                mvOutPendingMoves[itMap.first].emplace_back(iMovementUnit);
            }
        }
    }
    mvPendingMovesFromOutside.clear();
}

void Model::saveMovements(std::string fileName, int stepNum){
    Parameters& iParams = Parameters::getInstance();
    std::fstream f;
    f.open(fileName.c_str(), std::ios::app | std::ios::out);
    if (not f.is_open()) throw std::runtime_error("ERROR: can't open "+ fileName +" file.");
    std::string destStatus;
    std::string sourceStatus;
    for (auto& itm : mvOutPendingMoves){
        if (itm.first != iParams.outsideMetapop){
            destStatus = std::to_string(mpHerds.at(itm.first)->getStatus());
        } else {
            destStatus = "NA";
        }
        for (auto& itv : itm.second){
            if (itv.sourceId != iParams.outsideMetapop){
                sourceStatus = std::to_string(itv.sourceStatus);
            } else {
                sourceStatus = "NA";
            }
            f << itv.sourceId << "\t" << itm.first << "\t" << stepNum+1 << "\t" << itv.pAnimal->getAge() << "\t" << itv.pAnimal->getHealthStatus() << "\t" << sourceStatus << "\t" << destStatus << "\t" << itv.rewiringType << std::endl;
        }
    }
    f.close();
}

void Model::saveHeadcountForComparisonWithObserved(int runNum){
    Parameters& iParams = Parameters::getInstance();
    std::string fileName;
    fileName = iParams.resultsPath +"headcountPerHerdIdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream fHC(fileName.c_str());
    fileName = iParams.resultsPath +"cowPerHerdIdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream fHCCows(fileName.c_str());
    fileName = iParams.resultsPath +"persistencePerHerdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream fP(fileName.c_str());
    for (auto& it : mpHerds){
        fHC << it.first << "\t";
        for (auto nb : it.second->getHerdState()._vHeadcount) fHC << nb << "\t"; fHC << "\n";
        fHCCows << it.first << "\t";
        for (auto nb : it.second->getHerdState()._vCows) fHCCows << nb << "\t"; fHCCows << "\n";
        for (auto nb : it.second->getHerdState()._vPersistence) fP << nb << "\t"; fP << "\n";
    }
    fHC.close();
    fHCCows.close();
    fP.close();
}

void Model::saveResults(int runNum){
    Parameters& iParams = Parameters::getInstance();
    std::string fileName;
    fileName = iParams.resultsPath +"twoYearsOldPerHerdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream f2YO(fileName.c_str());
    fileName = iParams.resultsPath +"IhPerHerdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream fIh(fileName.c_str());
    fileName = iParams.resultsPath +"positiveIhPerHerdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream fPosIh(fileName.c_str());
    fileName = iParams.resultsPath +"infected2YOPerHerdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream fInf2YO(fileName.c_str());
    fileName = iParams.resultsPath +"persistencePerHerdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream fP(fileName.c_str());
    fileName = iParams.resultsPath +"statesPerHerdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream fSe(fileName.c_str());
    fileName = iParams.resultsPath +"statusPerHerdPerStep_run"+ std::to_string(runNum) +".csv";
    std::ofstream fSu(fileName.c_str());
    for (auto id : vHerdIds){
        RepetitionHerdState& iHerdState = mpHerds.at(id)->getHerdState();
        for (auto nb : iHerdState._vTwoYearsOld) f2YO << nb << "\t"; f2YO << "\n";
        for (auto nb : iHerdState._vIhtot) fIh << nb << "\t"; fIh << "\n";
        for (auto nb : iHerdState._vPositiveIhtot) fPosIh << nb << "\t"; fPosIh << "\n";
        for (auto nb : iHerdState._vInfectedTwoYearsOld) fInf2YO << nb << "\t"; fInf2YO << "\n";
        for (auto nb : iHerdState._vPersistence) fP << nb << "\t"; fP << "\n";
        for (auto nb : iHerdState._vHealthStates) fSe << nb << "\t"; fSe << "\n";
        for (auto nb : iHerdState._vStatus) fSu << nb << "\t"; fSu << "\n";
    }
    f2YO.close();
    fIh.close();
    fPosIh.close();
    fInf2YO.close();
    fP.close();
    fSe.close();
    fSu.close();
}

void Model::run(){
    Parameters& iParams = Parameters::getInstance();
    Metapop& iMetapop = Metapop::getInstance();
    int yearNum;
    std::string herdId;
    std::map<std::string, std::vector<MovementUnitFromData>>::iterator itMovementsAtStep;
    std::map<std::string, std::vector<MovementUnit>>::iterator itPendingMovesMap;
    std::map<std::string, std::vector<MovementUnit>> mvPendingMovesFromOutside;
    if (iParams.doSavingOfInitialInfection){
        std::remove(iParams.initOutFile.c_str());
    }
    for (auto runNum = 0; runNum < iParams.runsNb; ++runNum){
        std::cout << "run " << runNum << std::endl;
        std::remove((iParams.resultsPath + iParams.newMovesOutFile + std::to_string(runNum) +".txt").c_str());
        iMetapop.reset();
        iParams.initTime();
        if (iParams.doLoadingOfInitialPrev){
            initializeHerdsAccordingToPrev();
        } else if (iParams.propOfHerdsToInfect == 0 and not iParams.dontInfect and not iParams.doLoadingOfInitialInfection){
            initializeHerdsAccordingToStatus();
        } else {
            initializeHerds();
        }
        for (auto t = 1; t < iParams._totalTimeSteps; ++t){
            yearNum = t / iParams.weeksNbPerYear;
            auto stepNumForMoves = t-1;
            std::cout << "-- t " << t << std::endl;
            iParams._currentTime = t;
            if (iParams.useHerdHistory and yearNum > 1){ //if useHerdHistory, wait for the third year to rewire (3 status are needed: init+year0+year1)
                iParams.isRewiringAllowed = true;
            }
            for (auto& it : mpHerds){
                herdId = it.first;
                itPendingMovesMap = mvInPendingMoves.find(herdId);
                if (itPendingMovesMap != mvInPendingMoves.end()){
                    it.second->addPendingMoves(itPendingMovesMap->second);
                }
                it.second->runDynamics(t);
                itMovementsAtStep = vmvMovements[stepNumForMoves].find(herdId);
                if (itMovementsAtStep != vmvMovements[stepNumForMoves].end()){
                    it.second->sendMoves(itMovementsAtStep->second, mvOutPendingMoves, mvPendingMovesFromOutside);
                }
            }
            iMetapop.computeHealthStateProbaPerAgeGroup();
            if (iParams.isRewiringAllowed){            
                MovesRewiring iMovesRewiring;
                if (iParams.doRewiring){
                    iMovesRewiring.rewire(mpHerds, vHerdIdsToRewire, mvOutPendingMoves, mvPendingMovesFromOutside);
                } else if (iParams.doRewiringForBuyers){
                    iMovesRewiring.rewireBuyers(vHerdIdsToRewire, mvOutPendingMoves);
                } else if (iParams.doRewiringForSellers){
                    iMovesRewiring.rewireSellers(vHerdIdsToRewire, mvOutPendingMoves);
                }
            }
            sendMovesFromOutsideMetapop(stepNumForMoves, mvPendingMovesFromOutside);
            saveMovements(iParams.resultsPath + iParams.newMovesOutFile + std::to_string(runNum) +".txt", stepNumForMoves);
            mvInPendingMoves = mvOutPendingMoves;
            mvOutPendingMoves.clear();
            iMetapop.reset();
            iParams.updateWeekNum();
            if (iParams._weekNumber == 0 and yearNum < iParams.yearsNb){
                for (auto& it : mpHerds){
                    it.second->updateEachYear(yearNum);
                }
            }
        }
        if (iParams.dontInfect){
            saveHeadcountForComparisonWithObserved(runNum);
        } else {
            saveResults(runNum);
        }
        mpHerds.clear();
    }
}

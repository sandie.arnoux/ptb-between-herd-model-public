
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "Results.h"

Results* Results::s_instance = nullptr;

Results& Results::getInstance(){
    if (s_instance == nullptr){
        s_instance = new Results();
    }
    return *s_instance;
}

void Results::kill(){
    if (s_instance != nullptr){
        delete s_instance;
    }
    s_instance = nullptr;
}

Results::Results(){
}

void Results::initialize(){
    Parameters& iParams = Parameters::getInstance();
    _vvvOutputsPerRunPerStep.reserve(eLast);
    for (auto i = 0; i < eLast; ++i){
        _vvvOutputsPerRunPerStep.emplace_back(std::vector<std::vector<double>>(iParams.runsNb, std::vector<double>(iParams._totalTimeSteps, 0)));
    }
}

std::vector<std::vector<double>>& Results::operator[] (int outputNum){
    return _vvvOutputsPerRunPerStep[outputNum];
}

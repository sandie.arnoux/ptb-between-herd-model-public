
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <string>
#include <memory>

#include "herd/Animal.h"

class MovementUnitFromData {
public:
    std::string destinationId;
    int age;
    int breed;
    MovementUnitFromData(std::string destId, int ageAtMove, int breedOfMove){
        destinationId = destId;
        age = ageAtMove;
        breed = breedOfMove;
    }
};

enum RewiringTypes {
    //0: not rewired, 1: rewired in metapopulation, 2: rewired with "outside of metapopulation", 3: animal health state changed to S
    //4: equivalent animal not available in source herd so source will be from "outside" ("NA move"), 5: forbidden move (can't be rewired in metatpopulation) so source will be from "outside" ("broken move"),
    //6: "NA move" will come from an "outside" herd of the same or better status, 7: "broken move" will come from an "outside" herd of the same or better status
    eNotRewired, eRewiredInMetapop, eRewiredOutside, eHealthyAnimal, eNA, eBroken, eRewiredNA, eRewiredBroken
};

class MovementUnit {
public:
    std::string sourceId;
    int sourceStatus;
    int breed;
    std::shared_ptr<Animal> pAnimal;
    int rewiringType;
    MovementUnit(std::string id, int status, int breedOfMove, std::shared_ptr<Animal> pAni, int rewType){
        sourceId = id;
        sourceStatus = status;
        breed = breedOfMove;
        pAnimal = pAni;
        rewiringType = rewType;
    }
    bool operator==(const MovementUnit& iMovementUnit) const { //for tests
        return (sourceId == iMovementUnit.sourceId
                and sourceStatus == iMovementUnit.sourceStatus
                and breed == iMovementUnit.breed
                and pAnimal == iMovementUnit.pAnimal
                and rewiringType == iMovementUnit.rewiringType);
    }
};

class MovementForMatrix {
public:
    std::string sourceId;
    std::string destinationId;
    std::shared_ptr<Animal> pAnimal;
    MovementForMatrix(std::string source, std::string dest, std::shared_ptr<Animal> pAni){
        sourceId = source;
        destinationId = dest;
        pAnimal = pAni;
    }
    bool operator!=(const MovementForMatrix& iMovementForMatrix) const { //for tests
        return (sourceId != iMovementForMatrix.sourceId
                or destinationId != iMovementForMatrix.destinationId
                or pAnimal != iMovementForMatrix.pAnimal);
    }
};

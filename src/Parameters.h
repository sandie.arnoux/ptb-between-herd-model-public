
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <string>
#include <iostream>
#include <vector>
#include <random>

namespace enumAnimal {
    enum AgeGroup {newBorn, unweanedCalf, weanedInCalf, weanedOutCalf, youngHeifer, heifer, cow};
    // S : susceptible
    // It : infected transiently infectious
    // Il : infected latent (non infectious)
    // Im : infected moderately infectious
    // Ih : infected highly infectious (possibly clinical)
    enum HealthStatus {S, It, Il, Im, Ih};
    enum AnimalExitReason{noExit, death, culling};
    enum HerdStatus{clean, A, B, C, Cp};
}

enum HerdHealthState { //only 5 are defined, so vShedPropThres size have to be <= 5
    //WARNING: if useHerdHistory, then eA is a AAA herd and eB is all others, but still keep records of both types of herd statuses in outputs : eAAA is herd "status" and eA is herd "state"...
    eA, eB, eC, eD, eE
};

class Parameters {
protected:
    static Parameters *instance;
    Parameters();

public:
    std::mt19937 gen;
    int runsNb;
    int yearsNb;
    int weeksNbPerYear;
    int daysNbPerYear;
    bool dontInfect;
    std::vector<std::string> vDataFiles;
    std::string dataPath;
    std::string resultsPath;
    std::string initCondFile;
    std::string herdsToRewireFile;
    std::string selectedHerdsFile;
    std::string initOutFile;
    std::string initInFile;
    std::string initPrevFile;
    std::string newMovesOutFile;
    bool isRewiringAllowed;
    bool doRewiring;
    bool doRewiringForOutside;
    bool doRewiringForbiddenMoves;
    bool doRewiringForBuyers;
    bool doRewiringForSellers;
    bool useSelectedHerdsToRewire;
    float propOfHerdsToRewire;
    float defaultCalfExpo;
    bool useCalfExpoForSelectedHerds;
    float calfExpoForSelectedHerds;
    bool useCalfExpoForSpecHerds;
    int specHerdForCalfExpo;
    float calfExpoForSpecHerds;
    bool useSelectedHerds;
    bool doSavingOfInitialInfection;
    bool doLoadingOfInitialInfection;
    bool doLoadingOfInitialPrev;
    bool useBreed;
    int breedToRewire;
    bool useHerdHistory;
    int herdStatusNb;
    bool useAnnualPrev;
    bool useSameSensitivity;
    bool usePurchaseProba;
    float probaToPurchaseS;
    bool areDetectedIhCulledEarlier;
    float cullingProbaOfDetectedIh;
    float propOfHerdsToInfect;
    int maxInitPrev;
    std::vector<float> vShedPropThres; //thresholds of shedders proportion to classify herds
    std::vector<float> vHerdPropPerStatus; //herd proportion per status defined by vShedPropThres
    std::vector<int> vAgeThresForMoves; //thresholds of age to classify movements
    std::vector<int> vAgeThres; //thresholds of age to classify per age groups
    std::vector<int> vWeeksNbPerDeathRate;
    int categoriesNbForCows;
    std::string outsideMetapop;
    int minWeekToUpdateHerdState;
    int maxWeekToUpdateHerdState;
    void printAll();
    
    int _totalTimeSteps;
    int _weekNumber;
    int _currentTime;

    double stderrGaussian;
    double meanGaussian;

    int _ageWeaning;
    int _ageGrazing;
    int _ageYoungHeifer;
    int _ageHeifer;
    int _ageFirstCalving;
    int _nbStagesOfLactation;

    int _durSusceptible;
    int _durShedding;
    int _durIl;
    int _durIm;

    double _deathRateIm;
    double _deathRateIh;
    double _cullingRateIm;
    double _cullingRateIh;
    double _cullingProbaIh;
    double _deathRateBactInside;
    double _deathRateBactOutside;
    double _cleaningCP;
    double _adultsTogether;

    bool _colostrumTR;
    bool _milkTR;
    bool _localTR;
    double _susceptibilityCoeff;
    double _infectiousDose;
    double _infGeneralEnv;
    double _infLocalEnv;
    double _infGrazing;
    double _infIngestion;

    double _infinuteroIt;
    double _infinuteroIl;
    double _infinuteroIm;
    double _infinuteroIh;

    double _qtyFaecesCnw;
    double _qtyFaecesCw;
    double _qtyFaecesHf;
    double _qtyFaecesAd;
    double _qtyMilkDrunk;
    double _qtyColoDrunk;
    double _qtyMilkProd;
    double _qtyColoProd;
    double _propLactatingCows;
    double _qtyMilkProdIl;
    double _qtyMilkProdIm;
    double _qtyMilkProdIh;
    double _alphaFaecesIt;
    double _betaFaecesIt;
    double _alphaFaecesIm;
    double _betaFaecesIm;
    double _alphaFaecesIh;
    double _betaFaecesIh;

    double _alphaMilkDirIm;
    double _betaMilkDirIm;
    double _alphaMilkDirIh;
    double _betaMilkDirIh;
    double _alphaMilkIndirIm;
    double _betaMilkIndirIm;
    double _alphaMilkIndirIh;
    double _betaMilkIndirIh;
    double _alphaColoDirIm;
    double _betaColoDirIm;
    double _alphaColoDirIh;
    double _betaColoDirIh;
    double _alphaColoIndirIm;
    double _betaColoIndirIm;
    double _alphaColoIndirIh;
    double _betaColoIndirIh;
    double _propExcreMilkIm;
    double _propExcreMilkIh;
    double _propExcreColoIm;
    double _propExcreColoIh;

    double _testSensitivityIt;
    double _testSensitivityIl;
    double _testSensitivityIm;
    double _testSensitivityIh;
    double _testSpecificity;

    static Parameters& getInstance();
    static void kill();

    void updateAfterSetting();
    void initTime();
    void updateWeekNum();
};

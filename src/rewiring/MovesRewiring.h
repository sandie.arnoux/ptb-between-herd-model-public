
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <map>
#include <vector>
#include <string>
#include <memory>

#include "../Parameters.h"
#include "../MovementUnit.h"
#include "../herd/Herd.h"
#include "MovesMatrix.h"

class MovesRewiring {
private:
    std::vector<MovesMatrix> vMovesMatrices;

public:
    MovesRewiring();
    std::vector<MovesMatrix>& getMatricesVector(); //for tests
    void fillMatrices(std::map<std::string, std::shared_ptr<Herd>>& mpHerds, std::vector<std::string>& vHerdIdsToRewire, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves); //in public just for tests, but should be private...
    void rewire(std::map<std::string, std::shared_ptr<Herd>>& mpHerds, std::vector<std::string>& vHerdIdsToRewire, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves, std::map<std::string, std::vector<MovementUnit>>& mvPendingMovesFromOutside);
    void rewireBuyers(std::vector<std::string>& vHerdIdsToRewire, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves);
    void rewireSellers(std::vector<std::string>& vHerdIdsToRewire, std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves);
};


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "AbstractMovesMatrix.h"

AbstractMovesMatrix::AbstractMovesMatrix(){
	Parameters& iParams = Parameters::getInstance();
    for (auto i = 0; i < iParams.herdStatusNb; ++i){
        for (auto j = 0; j < iParams.herdStatusNb; ++j){
            if (i != j){
                std::vector<std::shared_ptr<MovementForMatrix>> v;
                mmvMovements[i].emplace(j, v);
            }
        }
    }
}

std::map<int, std::vector<std::shared_ptr<MovementForMatrix>>>& AbstractMovesMatrix::operator[] (int status){
    return mmvMovements[status];
}


/************************ functions used by tests ************************/
std::map<int, std::map<int, std::vector<std::shared_ptr<MovementForMatrix>>>>& AbstractMovesMatrix::getMatrix(){
    return mmvMovements;
}

bool AbstractMovesMatrix::isEmpty(){
    for (auto itsource : mmvMovements){
        for (auto itdest : itsource.second){
            if (itdest.second.size() != 0){
                return false;
            }
        }
    }
    return true;
}

bool AbstractMovesMatrix::operator==(AbstractMovesMatrix& iOtherMatrix){
    if (mmvMovements.size() != iOtherMatrix.getMatrix().size()){
        return false;
    }
    for (auto itmvMovements : mmvMovements){
        auto itmvOtherMatrix = iOtherMatrix.getMatrix().find(itmvMovements.first);
        if (itmvOtherMatrix == iOtherMatrix.getMatrix().end()){
            return false;
        } else {
            if (itmvMovements.second.size() != itmvOtherMatrix->second.size()){
                return false;
            }
            for (auto itvMovements : itmvMovements.second){
                auto itvOtherMatrix = itmvOtherMatrix->second.find(itvMovements.first);
                if (itvOtherMatrix == itmvOtherMatrix->second.end()){
                    return false;
                } else {
                    if (itvMovements.second.size() != itvOtherMatrix->second.size()){
                        return false;
                    }
                    for (auto i = 0; i < itvMovements.second.size(); ++i){
                        if (*itvMovements.second[i] != *itvOtherMatrix->second[i]){
                            return false;
                        }
                    }
                }
            }
        }
    }
    return true;
}

void AbstractMovesMatrix::print(){
    for (auto itsource : mmvMovements){
        std::cout << itsource.first << std::endl;
        for (auto itdest : itsource.second){
            std::cout << "\t" << itdest.first << std::endl;
            for (auto pMFM : itdest.second){
                std::cout << "\t\t" << pMFM->sourceId << " " << pMFM->destinationId << " " << pMFM->pAnimal << std::endl;
            }
        }
    }
}

void AbstractMovesMatrix::describe(){
    int linesNb = mmvMovements.size();
    int colsNb;
    int movesNb = 0;
    std::cout << "Moves number in each matrix cell:" << std::endl;
    for (auto itsource : mmvMovements){
        colsNb = itsource.second.size(); //TODO: check that matrix is square ?
        for (auto itdest : itsource.second){
            movesNb += itdest.second.size();
            std::cout << itdest.second.size() << " ";
        }
        std::cout << std::endl;
    }
    std::cout << "Lines nb = " << linesNb << ", cols nb = " << colsNb << ", total moves nb = " << movesNb << std::endl;
}
/*************************************************************************/


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include "MovesMatrix.h"

void MovesMatrix::rewire(std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves){
    Parameters& iParams = Parameters::getInstance();
    int n = mmvMovements.size();
    std::map<int, std::map<int, std::vector<std::shared_ptr<MovementForMatrix>>>>::iterator itSource, itRow;
    std::map<int, std::vector<std::shared_ptr<MovementForMatrix>>>::iterator itDest, itCol;
    int movesNbToMove;
    std::shared_ptr<MovementForMatrix> pMoveBadSource;
    std::shared_ptr<MovementForMatrix> pMoveGoodSource;
    for (auto destIndex = 0; destIndex < n-1; ++destIndex){
        for (auto sourceIndex = n-1; sourceIndex > destIndex; --sourceIndex){
            for (auto row = 0; row < destIndex+1; ++row){
                if (mmvMovements[sourceIndex][destIndex].size() == 0){
                    break;
                }
                for (auto col = n-1; col > sourceIndex-1; --col){
                    if (mmvMovements[sourceIndex][destIndex].size() == 0){
                        break;
                    }
                    if (mmvMovements[row][col].size() > 0){
                        movesNbToMove = std::min(mmvMovements[sourceIndex][destIndex].size(), mmvMovements[row][col].size());
                        for (auto count = 0; count < movesNbToMove; ++count){
                            pMoveBadSource = std::move(mmvMovements[sourceIndex][destIndex].back());
                            mmvMovements[sourceIndex][destIndex].pop_back();
                            pMoveGoodSource = std::move(mmvMovements[row][col].back());
                            mmvMovements[row][col].pop_back();
                            //find it in mvOutPendingMoves and move in it
                            for (auto it = mvOutPendingMoves[pMoveBadSource->destinationId].begin(); it != mvOutPendingMoves[pMoveBadSource->destinationId].end(); ){
                                //move with "bad source" will get a "bad destination" from move with "good source"
                                if (it->pAnimal == pMoveBadSource->pAnimal){
                                    if (it->sourceId != pMoveBadSource->sourceId){
                                        throw std::runtime_error("ERROR: in rewiring, matrix and original moves don't fit...");
                                    }
                                    it->rewiringType = eRewiredInMetapop;
                                    mvOutPendingMoves[pMoveGoodSource->destinationId].push_back(*it);
                                    it = mvOutPendingMoves[pMoveBadSource->destinationId].erase(it);
                                    break;
                                } else {
                                    ++it;
                                }
                            }
                            for (auto it = mvOutPendingMoves[pMoveGoodSource->destinationId].begin(); it != mvOutPendingMoves[pMoveGoodSource->destinationId].end(); ){
                                //move with "good source" will get a "good destination" from move with "bad source"
                                if (it->pAnimal == pMoveGoodSource->pAnimal){
                                    if (it->sourceId != pMoveGoodSource->sourceId){
                                        throw std::runtime_error("ERROR: in rewiring, matrix and original moves don't fit...");
                                    }
                                    it->rewiringType = eRewiredInMetapop;
                                    mvOutPendingMoves[pMoveBadSource->destinationId].push_back(*it);
                                    it = mvOutPendingMoves[pMoveGoodSource->destinationId].erase(it);
                                    break;
                                } else {
                                    ++it;
                                }
                            }
                            //exchange also in matrix
                            if (sourceIndex != col){
                                pMoveBadSource->destinationId = pMoveGoodSource->destinationId;
                                mmvMovements[sourceIndex][col].push_back(pMoveBadSource);
                            }
                            if (row != destIndex){
                                pMoveGoodSource->destinationId = pMoveBadSource->destinationId;
                                mmvMovements[row][destIndex].push_back(pMoveGoodSource);
                            }
                        }
                    }
                }
            }
        }
    }
}

void MovesMatrix::handleMovesImpossibleToRewire(std::map<std::string, std::vector<MovementUnit>>& mvOutPendingMoves, std::map<std::string, std::vector<MovementUnit>>& mvPendingMovesFromOutside){
    Parameters& iParams = Parameters::getInstance();
    for (auto sourceIndex = 1; sourceIndex < iParams.herdStatusNb; ++sourceIndex){
        for (auto destIndex = 0; destIndex < sourceIndex; ++destIndex){
            for (auto pMove : mmvMovements[sourceIndex][destIndex]){
                for (auto itMoveUnit = mvOutPendingMoves[pMove->destinationId].begin(); itMoveUnit != mvOutPendingMoves[pMove->destinationId].end(); ){
                    //it's not necessary here to check if useSelectedHerdsToRewire and (dest and source) in vHerdIdsToRewire, because moves won't be in matrix in the first place.
                    if (itMoveUnit->pAnimal == pMove->pAnimal){
                        if (itMoveUnit->sourceId != pMove->sourceId){
                            throw std::runtime_error("ERROR: in rewiring, matrix and original moves don't fit...");
                        }
                        //move in matrix is found in mvOutPendingMoves:
                        //source will send to outside, so nothing to do, but destination will receive from outside
                        //so change pMove source for "outside" and change animal state
                        itMoveUnit->rewiringType = eBroken;
                        itMoveUnit->sourceId = iParams.outsideMetapop;
                        itMoveUnit->sourceStatus = -1;
                        itMoveUnit->pAnimal->setHealthStatus(enumAnimal::S); //S by default, but animal state according to distribution in herds of same status...(cf sendMovesFromOutsideMetapop())
                        //save move in an other struct where all animals will get a health state according to distribution in herds of the same status as destination (so remove from mvOutPendingMoves). TODO: in this case, destination status is already known...to record ?
                        mvPendingMovesFromOutside[pMove->destinationId].push_back(*itMoveUnit); //would it be better to have pointer on MovementUnit in mvOutPendingMoves ?
                        itMoveUnit = mvOutPendingMoves[pMove->destinationId].erase(itMoveUnit);
                        break;
                    } else {
                        ++itMoveUnit;
                    }
                }
            }
        }
    }
}


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include <string>
#include <chrono>
#include <iostream>
#include <stdexcept>
#include <tclap/CmdLine.h>

#include "Parameters.h"
#include "Metapop.h"
#include "Results.h"
#include "Utils.h"
#include "Model.h"

int main(int argc, char** argv){
    TCLAP::CmdLine cmd("Simulate bovine paratuberculosis (PTB) spread between dairy herds in a region. It is data-driven and mechanistic.\nSee README for more information.", ' ', "0.1");
    TCLAP::ValueArg<int> iRunsNbArg("r", "runsNb", "number of repetitions (default=1)", false, 1, "int");
    TCLAP::ValueArg<float> iCalfExposureArg("e", "calfExposure", "calf rearing improvement is defined as a reduced exposure to the general environment by varying parameter from 1.0 to 0.35 (default=1.0)", false, 1.0, "float");
    TCLAP::ValueArg<float> iCullingRateIhArg("k", "cullingRateIh", "culling rate of detected Ih animals (default=1/26.)", false, 0.0384615, "float");
    TCLAP::ValueArg<float> iInfHerdsPropArg("p", "infHerdsProp", "proportion of herds to infect (default=0.). If 0., then proportion in each herd status will be used.", false, 0., "float");
    TCLAP::ValueArg<std::string> iWithinHerdPrevThresArg("t", "withinHerdPrevThres", "thresholds of infected animals proportion to classify herds (default='0.07;0.21', meaning 3 status A,B,C)", false, "0.07;0.21", "string");
    TCLAP::ValueArg<std::string> iHerdPropPerStatusArg("c", "herdPropPerStatus", "herd proportions in each status defined with -t (default='0.4;0.4;0.2', meaning 0.4 A, 0.4 B, 0.2 C)", false, "0.4;0.4;0.2", "string");
    TCLAP::ValueArg<int> iMaxInitPrevArg("x", "maxInitPrev", "maximum initial prevalence in all herds (default=90)", false, 90, "int");
    TCLAP::ValueArg<int> iRewiringTypeArg("w", "rewiringType", "0: no rewiring (default)\n1: rewiring according to shedders proportion\n2: buyers will receive only S animals\n3: sellers will send only S animals\nRewiring can be restricted to a subset of herds by combining -j and -l", false, 0, "int");
    TCLAP::ValueArg<std::string> iHerdsToRewireFilePathArg("l", "herdsToRewireFilePath", "file with list of herds selected for rewiring, 1 line = 1 herd id (default='herdsToRewire.txt')", false, "herdsToRewire.txt", "string");
    TCLAP::ValueArg<float> iHerdsPropToRewireArg("j", "herdsPropToRewire", "proportion of herds to rewire among herds listed in -l (default=0.). If 0., then -l won't be used. Warning: if 1., all herds in -l will be used for rewiring, but it can still be less than herds in input data...", false, 0., "float");
    TCLAP::ValueArg<float> iCalfExpoForSelectedArg("m", "calfExpoForSelected", "same as -e but for herds selected for rewiring, i.e. -j > 0 or for 'AAA' herds if -m < 1 and -q (default=1.0)", false, 1.0, "float");
    TCLAP::ValueArg<float> iCalfExpoForAHerdArg("", "calfExpoForAHerd", "same as -e but for herds with status A: they will keep this value even if their status is lost (default=1.0)", false, 1.0, "float");
    TCLAP::ValueArg<float> iCalfExpoForBHerdArg("", "calfExpoForBHerd", "same as -e but for herds with status B: they will keep this value even if their status is lost (default=1.0)", false, 1.0, "float");
    TCLAP::ValueArg<float> iCalfExpoForCHerdArg("", "calfExpoForCHerd", "same as -e but for herds with status C: they will keep this value even if their status is lost (default=1.0)", false, 1.0, "float");
    TCLAP::ValueArg<std::string> iOutDirectoryPathArg("o", "outDirectoryPath", "relative folder path to write all output files (default='../results/')", false, "../results/", "string");
    TCLAP::ValueArg<std::string> iSelectedHerdsFilePathArg("u", "selectedHerdsFilePath", "file with list of selected herds (only for -g for the moment), 1 line = 1 herd id (default='')", false, "", "string");
    TCLAP::ValueArg<std::string> iInitOutFilePathArg("y", "initOutFilePath", "if not empty, then save initial 'infection' in this file: 1 line = 1 infected herd with id, nbInfectedAnimals, prevalence and repetitionNum refering to a line in 'initCondFileName' (default='')", false, "", "string");
    TCLAP::ValueArg<std::string> iInitInFilePathArg("i", "initInFilePath", "if not empty, then load initial 'infection' from this file: 1 line = 1 infected herd with id, nbInfectedAnimals, prevalence and repetitionNum refering to a line in 'initCondFileName' (default='')", false, "", "string");
    TCLAP::ValueArg<std::string> iInitPrevFilePathArg("d", "initPrevFilePath", "if not empty, then each herd will be initialized with a prevalence from this file: 1 line = 1 integer which is the prevalence for this herd (same order as in data) (default='')", false, "", "string");
    TCLAP::ValueArg<float> iTestSensitivityArg("s", "testSensitivity", "test sensitivity to use for all animal health states. NOTE: herd status is defined by Bin(infected 2 years old animals nb, sensitivity). (default=1.0)", false, 1.0, "float");
    TCLAP::ValueArg<float> iProbaToPurchaseSArg("v", "probaToPurchaseS", "probability to purchase a susceptible animal for AAA herds (used with herd history (see -q), default=1.0)", false, 1.0, "float");
    cmd.add(iProbaToPurchaseSArg);
    TCLAP::SwitchArg iUsePurchaseProba("f", "usePurchaseProba", "a fixed probability to purchase a susceptible animal will be used (see -v), instead of the prevalence in the source herd (used with herd history)", cmd, false);
    cmd.add(iTestSensitivityArg);
    TCLAP::SwitchArg iUseSameSensitivity("", "useSameSensitivity", "same test sensitivity for all animal health states (see -s). By default, test sensitivity is It&Il=0.15, Im=0.47, Ih=0.71", cmd, false);
    TCLAP::SwitchArg iUseAnnualPrev("a", "useAnnualPrev", "herd state will be updated once a year (see -s), instead of an update per time step (used with rewiring)", cmd, false);
    TCLAP::SwitchArg iUseHerdHistory("q", "useHerdHistory", "herd status will be A if the 3 last states were A (see -s), and B otherwise (used with rewiring)", cmd, false);
    TCLAP::SwitchArg iUseBreed("b", "useBreed", "rewiring can only be done among Prim'Holstein (66)", cmd, false);
    cmd.add(iInitPrevFilePathArg);
    cmd.add(iInitInFilePathArg);
    cmd.add(iInitOutFilePathArg);
    cmd.add(iSelectedHerdsFilePathArg);
    TCLAP::SwitchArg iDontInfectSelectedHerds("g", "dontInfectSelectedHerds", "selected herds (-u) will be initialized without infection", cmd, false);
    TCLAP::SwitchArg iDoRewiringForbiddenMoves("", "doRewiringForbiddenMoves", "moves that can't be rewired will be done with an 'outside' herd of the same status (same as -z, but for forbidden moves and when no matching animal was found)", cmd, false);
    TCLAP::SwitchArg iDoRewiringForOutside("z", "doRewiringForOutside", "animals from outside will be considered coming from a herd of the same status", cmd, false);
    cmd.add(iOutDirectoryPathArg);
    cmd.add(iCalfExpoForCHerdArg);
    cmd.add(iCalfExpoForBHerdArg);
    cmd.add(iCalfExpoForAHerdArg);
    cmd.add(iCalfExpoForSelectedArg);
    cmd.add(iHerdsPropToRewireArg);
    cmd.add(iHerdsToRewireFilePathArg);
    cmd.add(iRewiringTypeArg);
    cmd.add(iMaxInitPrevArg);
    cmd.add(iHerdPropPerStatusArg);
    cmd.add(iWithinHerdPrevThresArg);
    cmd.add(iInfHerdsPropArg);
    cmd.add(iCullingRateIhArg);
    cmd.add(iCalfExposureArg);
    TCLAP::SwitchArg iNoInfection("n", "noInfection", "all herds will be initialized without infected animals, so no PTB spread", cmd, false);
    cmd.add(iRunsNbArg);
    cmd.parse(argc, argv);

	Parameters& iParams = Parameters::getInstance();
    iParams.runsNb = iRunsNbArg.getValue();
    iParams.dontInfect = iNoInfection.getValue();
    iParams.defaultCalfExpo = iCalfExposureArg.getValue();
    if (iCullingRateIhArg.getValue() > 0.0384615){  //6 months => 1/(52/2) = 0.0384615
        iParams.areDetectedIhCulledEarlier = true;
        iParams.cullingProbaOfDetectedIh = 1 - std::exp(-(iCullingRateIhArg.getValue()));   //1 month => 1/(52/12) = 0.230769
    }
    iParams.propOfHerdsToInfect = iInfHerdsPropArg.getValue();
    iParams.maxInitPrev = iMaxInitPrevArg.getValue();
    iParams.herdsToRewireFile = iHerdsToRewireFilePathArg.getValue();
    iParams.propOfHerdsToRewire = iHerdsPropToRewireArg.getValue();
    iParams.resultsPath = iOutDirectoryPathArg.getValue();
    iParams.doRewiringForOutside = iDoRewiringForOutside.getValue();
    iParams.doRewiringForbiddenMoves = iDoRewiringForbiddenMoves.getValue();
    iParams.useSelectedHerds = iDontInfectSelectedHerds.getValue();
    iParams.selectedHerdsFile = iSelectedHerdsFilePathArg.getValue();
    iParams.useBreed = iUseBreed.getValue();
    iParams.useHerdHistory = iUseHerdHistory.getValue();
    if (iParams.useHerdHistory){
        iParams.isRewiringAllowed = false;
    }
    iParams.useAnnualPrev = iUseAnnualPrev.getValue();
    iParams.useSameSensitivity = iUseSameSensitivity.getValue();
    iParams.usePurchaseProba = iUsePurchaseProba.getValue();
    iParams.probaToPurchaseS = iProbaToPurchaseSArg.getValue();
    int seed = 0;
    if (seed == 0){
        iParams.gen.seed(std::chrono::high_resolution_clock::now().time_since_epoch().count());
    } else {
        iParams.gen.seed(seed);
    }

    iParams.vShedPropThres = Utils::splitToFloatVector(iWithinHerdPrevThresArg.getValue(), ';');
    if (iParams.vShedPropThres.size() > 4){ //HerdHealthState enum size = 5, and another threshold is added (see 3 lines below)
        throw std::runtime_error("ERROR: within-herd prevalence thresholds number (-t) can't be > 4 (no more than 5 herd status are considered, because results are not different above 4 status)");
    }
    std::sort(std::begin(iParams.vShedPropThres), std::end(iParams.vShedPropThres));
    iParams.vShedPropThres.push_back(1.0);
    iParams.vShedPropThres.shrink_to_fit();
    if (iParams.useHerdHistory){
        iParams.herdStatusNb = 2;
    } else {
        iParams.herdStatusNb = iParams.vShedPropThres.size();
    }
    if (iParams.useSameSensitivity){
        iParams._testSensitivityIt = iTestSensitivityArg.getValue();
        iParams._testSensitivityIl = iTestSensitivityArg.getValue();
        iParams._testSensitivityIm = iTestSensitivityArg.getValue();
        iParams._testSensitivityIh = iTestSensitivityArg.getValue();
    }
    std::cout << "There will be " << iParams.herdStatusNb << " herd status, based on the thresholds: ";
    for (auto th : iParams.vShedPropThres) {std::cout << th << " ";}
    std::cout << "(status will be updated ";
    if (iParams.useAnnualPrev) {std::cout << "yearly";}
    else {std::cout << "weekly";}
    if (not iParams.useSameSensitivity) {std::cout << " based on specific test sensitivity aka 'apparent prevalence')" << std::endl;}
    else if (iTestSensitivityArg.getValue() == 1) {std::cout << " based on 'true prevalence')" << std::endl;}
    else {std::cout << " based on unique test sensitivity "<< iTestSensitivityArg.getValue() << ")" << std::endl;}

    if (iParams.dontInfect){
        iParams.propOfHerdsToInfect = 0;
    } else if (iParams.propOfHerdsToInfect == 0){ //then option '-c' will be used (or '-i' or '-d')
        iParams.vHerdPropPerStatus = Utils::splitToFloatVector(iHerdPropPerStatusArg.getValue(), ';');
        if (iParams.vShedPropThres.size() != iParams.vHerdPropPerStatus.size()){
            throw std::runtime_error("ERROR: sizes of withinHerdPrevThres and herdPropPerStatus don't fit. If 3 thresholds, then 4 proportions should be given.");
        }
        auto propSum = std::accumulate(std::begin(iParams.vHerdPropPerStatus), std::end(iParams.vHerdPropPerStatus), 0.);
        if (propSum - 1 > 1.e-06){
            throw std::runtime_error("ERROR: sum of proportions in herdPropPerStatus should be equal to 1.");
        }
    } else { //then option '-p' or '-i' or '-d' will be used
        iParams.vHerdPropPerStatus.clear();
    }
    int rewiringType = iRewiringTypeArg.getValue();
    if (rewiringType == 1){
        iParams.doRewiring = true;
    } else if (rewiringType == 2) {
        iParams.doRewiringForBuyers = true;
    } else if (rewiringType == 3) {
        iParams.doRewiringForSellers = true;
    }
    if (iParams.propOfHerdsToRewire > 0.){
        iParams.useSelectedHerdsToRewire = true;
        iParams.calfExpoForSelectedHerds = iCalfExpoForSelectedArg.getValue();
    } else if (iCalfExpoForSelectedArg.getValue() < 1.0){
        iParams.useCalfExpoForSelectedHerds = true;
        iParams.calfExpoForSelectedHerds = iCalfExpoForSelectedArg.getValue();
    }
    if (iCalfExpoForAHerdArg.getValue() < 1.0){
        iParams.useHerdHistory = false;
        iParams.useCalfExpoForSelectedHerds = false;
        iParams.useCalfExpoForSpecHerds = true;
        iParams.specHerdForCalfExpo = eA;
        iParams.calfExpoForSpecHerds = iCalfExpoForAHerdArg.getValue();
    } else if (iCalfExpoForBHerdArg.getValue() < 1.0){
        iParams.useHerdHistory = false;
        iParams.useCalfExpoForSelectedHerds = false;
        iParams.useCalfExpoForSpecHerds = true;
        iParams.specHerdForCalfExpo = eB;
        iParams.calfExpoForSpecHerds = iCalfExpoForBHerdArg.getValue();
    } else if (iCalfExpoForCHerdArg.getValue() < 1.0){
        iParams.useHerdHistory = false;
        iParams.useCalfExpoForSelectedHerds = false;
        iParams.useCalfExpoForSpecHerds = true;
        iParams.specHerdForCalfExpo = eC;
        iParams.calfExpoForSpecHerds = iCalfExpoForCHerdArg.getValue();
    }
    if (iInitOutFilePathArg.getValue() != ""){
        iParams.doSavingOfInitialInfection = true;
        iParams.initOutFile = iInitOutFilePathArg.getValue();
        iParams.runsNb = 1;
    }
    if (iInitInFilePathArg.getValue() != ""){
        iParams.doLoadingOfInitialInfection = true;
        iParams.initInFile = iInitInFilePathArg.getValue();
    }
    if (iInitPrevFilePathArg.getValue() != ""){
        iParams.doLoadingOfInitialPrev = true;
        iParams.initPrevFile = iInitPrevFilePathArg.getValue();
    }
    iParams.updateAfterSetting();

    Model iModel = Model();
    iModel.loadDemography();
    iModel.loadMovements();
    if (iParams.useSelectedHerdsToRewire){
        iModel.loadHerdsToRewire();
    }
    if (iParams.useSelectedHerds){
        iModel.loadSelectedHerds();
    }
    if (iParams.doLoadingOfInitialInfection){
        iModel.loadInitialInfection();
    }
    if (iParams.doLoadingOfInitialPrev){
        iModel.loadInitialPrevalence();
    }
    iModel.loadInitPrevMatrix();
    iModel.run();

    Results::kill();
    Metapop::kill();
    Parameters::kill();

    return EXIT_SUCCESS;
}

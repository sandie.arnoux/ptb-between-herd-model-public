
// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#pragma once
#include <string>
#include <sstream>
#include <random>
#include <iterator>
#include <algorithm>

#include "Parameters.h"
#include "BetaDistribution.h"

namespace Utils {
    static std::string getFirstWord(const std::string &s, char separator){
        std::string word;
        std::stringstream ss(s);
        std::getline(ss, word, separator);
        return word;
    }

    template<typename Out>
        static void split(const std::string &s, char separator, Out result) {
            std::stringstream ss;
            ss.str(s);
            std::string item;
            while (std::getline(ss, item, separator)) {
                *(result++) = item;
            }
        }
    static std::vector<std::string> split(const std::string &s, char separator) {
        std::vector<std::string> vStrings;
        split(s, separator, std::back_inserter(vStrings));
        return vStrings;
    }

    template<typename Out>
        static void split_int(std::string &s, char separator, Out result) {
            std::stringstream ss;
            ss.str(s);
            std::string item;
            while (std::getline(ss, item, separator)) {
                *(result++) = std::stoi(item);
            }
        }
    static std::vector<int> splitWithoutFirstItem(std::string &s, char separator) {
        s = s.substr(s.find_first_of("\t") + 1);
        std::vector<int> v;
        split_int(s, separator, std::back_inserter(v));
        return v;
    }

    template<typename Out>
        static void split_float(std::string &s, char separator, Out result) {
            std::stringstream ss;
            ss.str(s);
            std::string item;
            while (std::getline(ss, item, separator)) {
                *(result++) = std::stof(item);
            }
        }
    static std::vector<float> splitWithoutFirstItem_float(std::string &s, char separator) {
        s = s.substr(s.find_first_of("\t") + 1);
        std::vector<float> v;
        split_float(s, separator, std::back_inserter(v));
        return v;
    }
    static std::vector<float> splitToFloatVector(std::string &s, char separator) {
        std::vector<float> v;
        split_float(s, separator, std::back_inserter(v));
        return v;
    }

    static void accumulateInFirstVector(std::vector<int>& v1, std::vector<int>& v2){
        std::transform(v1.begin(), v1.end(), v2.begin(), v1.begin(), std::plus<int>());
    }

    static void accumulateInFirstVector(std::vector<double>& v1, std::vector<double>& v2){
        std::transform(v1.begin(), v1.end(), v2.begin(), v1.begin(), std::plus<double>());
    }

    static void multiplyInFirstVector(std::vector<double>& v1, std::vector<double>& v2){
        std::transform(v1.begin(), v1.end(), v2.begin(), v1.begin(), std::multiplies<double>());
    }
 
    static void divideInPlace(std::vector<double>& v, double nb){
        if (nb == 0){
            std::fill(v.begin(), v.end(), 0);
        } else {
            for (auto i = 0; i < v.size(); i++){
                    v[i] = v[i] / nb;
            }
        }
    }

    static std::vector<double> transformItemsByIsNotZero(std::vector<int>& v){
        std::vector<double> vZeroOne;
        vZeroOne.reserve(v.size());
        for (auto i : v){
            vZeroOne.push_back((i > 0) ? 1 : 0);
        }
        return vZeroOne;
    }

    template<typename Iter, typename RandomGenerator>
        static Iter select_randomly(Iter start, Iter end, RandomGenerator& g) {
            std::uniform_int_distribution<> dis(0, std::distance(start, end) - 1);
            std::advance(start, dis(g));
            return start;
        }
    template<typename Iter>
        static Iter select_randomly(Iter start, Iter end) {
            Parameters& iParams = Parameters::getInstance();
            return select_randomly(start, end, iParams.gen);
        }

    static bool bernoulli(const double p){
        Parameters& iParams = Parameters::getInstance();
        std::bernoulli_distribution distribution(p);
        return distribution(iParams.gen);
    }

    static int uniform(const int max){
        Parameters& iParams = Parameters::getInstance();
        std::uniform_int_distribution<> distribution(0, max-1);
        return distribution(iParams.gen);
    }

    static int uniform(const int min, const int max){ //WARNING: both min and max are inclusive !!!
        Parameters& iParams = Parameters::getInstance();
        std::uniform_int_distribution<> distribution(min, max);
        return distribution(iParams.gen);
    }

    static double beta(const double alpha, const double beta){
        Parameters& iParams = Parameters::getInstance();
        sftrabbit::beta_distribution<> distribution(alpha, beta);
        return distribution(iParams.gen);
    }

    static double gaussian(const double mean, const double stddev){
        Parameters& iParams = Parameters::getInstance();
        std::normal_distribution<> distribution(mean, stddev);
        return distribution(iParams.gen);
    }

    static int binomial(const int nb, const double p){
        Parameters& iParams = Parameters::getInstance();
        std::binomial_distribution<> distribution(nb, p);
        return distribution(iParams.gen);
    }

    static std::vector<int> multinomial(const int N, const std::vector<double>& vProbs){
        std::vector<int> vNbs;
        vNbs.resize(vProbs.size(), 0);
        double sumProbs = 0.0;
        int sumNbs = 0;
        double norm = std::accumulate(vProbs.begin(), vProbs.end(), 0.0);
        for (auto i = 0; i < vProbs.size(); i++){
            if(vProbs[i] > 0.0){
                vNbs[i] = binomial(N - sumNbs, vProbs[i] / (norm - sumProbs));
            } else {
                vNbs[i] = 0;
            }
            sumProbs += vProbs[i];
            sumNbs += vNbs[i];
            if (N - sumNbs == 0){
                break;
            }
        }
        return vNbs;
    }

    static std::vector<std::string> randomSamplingWithoutReplacement(std::vector<std::string>& vData, int nbToSelect){
        int max = vData.size() - 1;
        int indexSelected;
        std::vector<std::string> vDataSelected;
        for (auto i = 0; i < nbToSelect; i++) {
            indexSelected = uniform(max);
            std::swap(vData[indexSelected], vData[max]);
            vDataSelected.emplace_back(vData[max]);
            max--;
        }
        return vDataSelected;
    }
}


// PTB-rewiring (bovine paratuberculosis and movements rewiring)
// =============================================================
// 
// Contributors and contact:
// -------------------------
// 
//     - Sandie Arnoux (sandie.arnoux@inrae.fr)
//     - Pauline Ezanno (pauline.ezanno@inrae.fr)
// 
//     INRAE, Oniris, BIOEPAR, 44300, Nantes, France
// 
// 
// How to cite:
// ------------
// 
//     P. Ezanno, S. Arnoux, A. Joly, R. Vermesse (2021). "Risk-based trade
//     movements to control bovine paratuberculosis at a regional scale",
//     submitted in Preventive Veterinary Medicine.
// 
// 
// License:
// --------
// 
//    Copyright 2018 INRAE
// 
//    Licensed under the Apache License, Version 2.0 (the "License");
//    you may not use this file except in compliance with the License.
//    You may obtain a copy of the License at
// 
//        http://www.apache.org/licenses/LICENSE-2.0
// 
//    Unless required by applicable law or agreed to in writing, software
//    distributed under the License is distributed on an "AS IS" BASIS,
//    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
//    See the License for the specific language governing permissions and
//    limitations under the License.

#include <UnitTest++.h>
#include <string>
#include <vector>
#include <memory>

#include "../rewiring/MovesMatrix.h"
#include "../MovementUnit.h"
#include "../herd/Animal.h"

// . . x
// . . .
// x . .
TEST(rewire_oneRewiring_twoMovesToSameStatus){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, pAnimal1, 0)); //A sells to C
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eC, 66, pAnimal2, 0)); //C sells to A
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eA][eC].push_back(pMove1);
    iMovesMatrix[eC][eA].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eA, 66, pAnimal1, 1)); //A sells to A
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eC, 66, pAnimal2, 1)); //C sells to C

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(iMovesMatrix.isEmpty());
}

// . . x
// x . .
// . . .
TEST(rewire_oneRewiring_twoMovesToDiffStatus){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 0)); //B sells to A
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 0)); //A sells to C
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eB][eA].push_back(pMove1);
    iMovesMatrix[eA][eC].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 1)); //B sells to C
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 1)); //A sells to A
    auto expMove1 = std::make_shared<MovementForMatrix>("source00", "dest01", pAnimal1);
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eB][eC].push_back(expMove1);

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(expMovesMatrix == iMovesMatrix);
}

// . . x
// . . .
// . x .
TEST(rewire_oneRewiring_twoMovesToDiffStatus_lowerStatus){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 0)); //C sells to B
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 0)); //A sells to C
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eC][eB].push_back(pMove1);
    iMovesMatrix[eA][eC].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 1)); //A sells to B
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 1)); //C sells to C
    auto expMove1 = std::make_shared<MovementForMatrix>("source01", "dest00", pAnimal2);
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eA][eB].push_back(expMove1);

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(expMovesMatrix == iMovesMatrix);
}

// . x .
// . . .
// x . .
TEST(rewire_noRewiring){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 0)); //C sells to A
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 0)); //A sells to B
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eC][eA].push_back(pMove1);
    iMovesMatrix[eA][eB].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 0)); //C sells to A
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 0)); //A sells to B
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eC][eA].push_back(pMove1);
    expMovesMatrix[eA][eB].push_back(pMove2);

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(expMovesMatrix == iMovesMatrix);
}

// . x .
// . . .
// . x .
TEST(rewire_noRewiring2){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 0)); //C sells to B
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 0)); //A sells to B
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eC][eB].push_back(pMove1);
    iMovesMatrix[eA][eB].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 0)); //C sells to B
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 0)); //A sells to B
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eC][eB].push_back(pMove1);
    expMovesMatrix[eA][eB].push_back(pMove2);

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(expMovesMatrix == iMovesMatrix);
}

// . x .
// x . .
// . . .
TEST(rewire_oneRewiring_sameStatus){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 0)); //B sells to A
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 0)); //A sells to B
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eB][eA].push_back(pMove1);
    iMovesMatrix[eA][eB].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 1)); //B sells to B
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 1)); //A sells to A

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(iMovesMatrix.isEmpty());
}

// . . .
// x . x
// . . .
TEST(rewire_noRewiring3){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 0)); //B sells to A
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eB, 66, pAnimal2, 0)); //B sells to C
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eB][eA].push_back(pMove1);
    iMovesMatrix[eB][eC].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 0)); //B sells to A
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eB, 66, pAnimal2, 0)); //B sells to C
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eB][eA].push_back(pMove1);
    expMovesMatrix[eB][eC].push_back(pMove2);

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(expMovesMatrix == iMovesMatrix);
}

// . . .
// . . x
// x . .
TEST(rewire_noRewiring_diffStatus_maybeToRewire){ //no rewiring, but should we ? B sells to A is better than C sells to A, no ?
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 0)); //C sells to A
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eB, 66, pAnimal2, 0)); //B sells to C
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eC][eA].push_back(pMove1);
    iMovesMatrix[eB][eC].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 0)); //C sells to A
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eB, 66, pAnimal2, 0)); //B sells to C
    auto expMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto expMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eC][eA].push_back(expMove1);
    expMovesMatrix[eB][eC].push_back(expMove2);

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(expMovesMatrix == iMovesMatrix);
}

// . . .
// . . x
// . x .
TEST(rewire_oneRewiring_sameStatus2){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 0)); //C sells to B
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eB, 66, pAnimal2, 0)); //B sells to C
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eC][eB].push_back(pMove1);
    iMovesMatrix[eB][eC].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eB, 66, pAnimal2, 1)); //B sells to B
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eC, 66, pAnimal1, 1)); //C sells to C

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(iMovesMatrix.isEmpty());
}

// . x .
// . . x
// . . .
TEST(rewire_noRewiring_diffStatus){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 0)); //B sells to C
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 0)); //A sells to B
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eB][eC].push_back(pMove1);
    iMovesMatrix[eA][eB].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 0)); //B sells to C
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eA, 66, pAnimal2, 0)); //A sells to B
    auto expMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto expMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eB][eC].push_back(expMove1);
    expMovesMatrix[eA][eB].push_back(expMove2);

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(expMovesMatrix == iMovesMatrix);
}

// . . .
// x . .
// . x .
TEST(rewire_noRewiring_diffStatus2){
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 0)); //B sells to A
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eC, 66, pAnimal2, 0)); //C sells to B
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eB][eA].push_back(pMove1);
    iMovesMatrix[eC][eB].push_back(pMove2);

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eB, 66, pAnimal1, 0)); //B sells to A
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eC, 66, pAnimal2, 0)); //C sells to B
    auto expMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto expMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eB][eA].push_back(expMove1);
    expMovesMatrix[eC][eB].push_back(expMove2);

    iMovesMatrix.rewire(mvOutPendingMoves);

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(expMovesMatrix == iMovesMatrix);
}

//Should we test also the following ? But same source status or dest status, no rewiring (already 2 examples tested)
// . . .   . . .   . . x   . x x
// . . .   x . .   . . x   . . .
// x x .   x . .   . . .   . . .

TEST(rewire_global){
    std::vector<std::shared_ptr<Animal>> vpAnimal;
    for (auto i = 0; i < 30; ++i){
        vpAnimal.emplace_back(std::make_shared<Animal>(nullptr, 20, 2, 0)); //age class should be the same (one matrix), but health status could be different
    }
    //dest00    eA
    //dest01    eC
    //dest02    eC
    //dest04    eD
    //dest05    eD
    //dest06    eD
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[0], 0));
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[1], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[2], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[3], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[4], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[5], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[6], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[7], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[8], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[9], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[10], 0));
    mvOutPendingMoves["dest05"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[11], 0));
    mvOutPendingMoves["dest06"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[12], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source02", eB, 66, vpAnimal[13], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[14], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[15], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[16], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[17], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[18], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source04", eC, 66, vpAnimal[19], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source05", eD, 66, vpAnimal[20], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source06", eD, 66, vpAnimal[21], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source07", eD, 66, vpAnimal[22], 0));
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source08", eD, 66, vpAnimal[23], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source09", eD, 66, vpAnimal[24], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source10", eD, 66, vpAnimal[25], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source11", eD, 66, vpAnimal[26], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source12", eD, 66, vpAnimal[27], 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source13", eD, 66, vpAnimal[28], 0));
    mvOutPendingMoves["dest04"].emplace_back(MovementUnit("source14", eD, 66, vpAnimal[29], 0));
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest01", vpAnimal[1]));
    iMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[2]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[3]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source00", "dest04", vpAnimal[4]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest04", vpAnimal[5]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[6]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[7]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[8]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[9]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[10]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest05", vpAnimal[11]));
    iMovesMatrix[eA][eD].push_back(std::make_shared<MovementForMatrix>("source01", "dest06", vpAnimal[12]));
    iMovesMatrix[eB][eC].push_back(std::make_shared<MovementForMatrix>("source02", "dest02", vpAnimal[13]));
    iMovesMatrix[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[14]));
    iMovesMatrix[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[15]));
    iMovesMatrix[eC][eA].push_back(std::make_shared<MovementForMatrix>("source03", "dest00", vpAnimal[16]));
    iMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source03", "dest04", vpAnimal[18]));
    iMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source04", "dest04", vpAnimal[19]));
    iMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source05", "dest00", vpAnimal[20]));
    iMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source06", "dest00", vpAnimal[21]));
    iMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source07", "dest00", vpAnimal[22]));
    iMovesMatrix[eD][eA].push_back(std::make_shared<MovementForMatrix>("source08", "dest00", vpAnimal[23]));
    iMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source09", "dest02", vpAnimal[24]));
    iMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source10", "dest02", vpAnimal[25]));
    iMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source11", "dest02", vpAnimal[26]));
    iMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source12", "dest02", vpAnimal[27]));
    iMovesMatrix[eD][eC].push_back(std::make_shared<MovementForMatrix>("source13", "dest02", vpAnimal[28]));

    std::map<std::string, std::vector<MovementUnit>> expOutPendingMoves;
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[0], 0));
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[12], 1));
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[11], 1));
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[10], 1));
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[9], 1));
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[8], 1));
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[7], 1));
    expOutPendingMoves["dest00"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[6], 1));
    expOutPendingMoves["dest01"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[1], 0));
    expOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[2], 0));
    expOutPendingMoves["dest02"].emplace_back(MovementUnit("source02", eB, 66, vpAnimal[13], 0));
    expOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[17], 0));
    expOutPendingMoves["dest02"].emplace_back(MovementUnit("source01", eA, 66, vpAnimal[5], 1));
    expOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[4], 1));
    expOutPendingMoves["dest02"].emplace_back(MovementUnit("source00", eA, 66, vpAnimal[3], 1));
    expOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[14], 1));
    expOutPendingMoves["dest02"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[15], 1));
    expOutPendingMoves["dest04"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[18], 0));
    expOutPendingMoves["dest04"].emplace_back(MovementUnit("source04", eC, 66, vpAnimal[19], 0));
    expOutPendingMoves["dest04"].emplace_back(MovementUnit("source14", eD, 66, vpAnimal[29], 0));
    expOutPendingMoves["dest04"].emplace_back(MovementUnit("source13", eD, 66, vpAnimal[28], 1));
    expOutPendingMoves["dest04"].emplace_back(MovementUnit("source12", eD, 66, vpAnimal[27], 1));
    expOutPendingMoves["dest04"].emplace_back(MovementUnit("source11", eD, 66, vpAnimal[26], 1));
    expOutPendingMoves["dest05"].emplace_back(MovementUnit("source07", eD, 66, vpAnimal[22], 1));
    expOutPendingMoves["dest05"].emplace_back(MovementUnit("source06", eD, 66, vpAnimal[21], 1));
    expOutPendingMoves["dest05"].emplace_back(MovementUnit("source05", eD, 66, vpAnimal[20], 1));
    expOutPendingMoves["dest05"].emplace_back(MovementUnit("source03", eC, 66, vpAnimal[16], 1));
    expOutPendingMoves["dest05"].emplace_back(MovementUnit("source10", eD, 66, vpAnimal[25], 1));
    expOutPendingMoves["dest05"].emplace_back(MovementUnit("source09", eD, 66, vpAnimal[24], 1));
    expOutPendingMoves["dest06"].emplace_back(MovementUnit("source08", eD, 66, vpAnimal[23], 1));
    MovesMatrix expMovesMatrix = MovesMatrix();
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest01", vpAnimal[1]));
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[2]));
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source01", "dest02", vpAnimal[5]));
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[4]));
    expMovesMatrix[eA][eC].push_back(std::make_shared<MovementForMatrix>("source00", "dest02", vpAnimal[3]));
    expMovesMatrix[eB][eC].push_back(std::make_shared<MovementForMatrix>("source02", "dest02", vpAnimal[13]));
    expMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source03", "dest04", vpAnimal[18]));
    expMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source04", "dest04", vpAnimal[19]));
    expMovesMatrix[eC][eD].push_back(std::make_shared<MovementForMatrix>("source03", "dest05", vpAnimal[16]));
    expMovesMatrix[eA][eD]; //empty, but exists
    expMovesMatrix[eB][eD];
    expMovesMatrix[eD][eA];
    expMovesMatrix[eD][eB];
    expMovesMatrix[eD][eC];

    iMovesMatrix.rewire(mvOutPendingMoves);

//    for (auto itmap : mvOutPendingMoves){
//        std::cout << itmap.first << std::endl;
//        for (auto iMU : itmap.second){
//            std::cout << "\t" << iMU.sourceId << " " << iMU.sourceStatus << " " << iMU.pAnimal << " " << iMU.rewiringType << std::endl;
//        }
//    }
//    std::cout << "---" << std::endl;
//    for (auto itmap : expOutPendingMoves){
//        std::cout << itmap.first << std::endl;
//        for (auto iMU : itmap.second){
//            std::cout << "\t" << iMU.sourceId << " " << iMU.sourceStatus << " " << iMU.pAnimal << " " << iMU.rewiringType << std::endl;
//        }
//    }
//    std::cout << "***" << std::endl;
//    iMovesMatrix.print();
//    std::cout << "---" << std::endl;
//    expMovesMatrix.print();

    CHECK(expOutPendingMoves == mvOutPendingMoves);
    CHECK(expMovesMatrix == iMovesMatrix);
}

TEST(rewire_badMatrix){ //something went wrong in fillMatrices() or just after...
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, pAnimal1, 0));
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eC, 66, pAnimal2, 0));
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source00", "dest01", pAnimal2);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eA][eC].push_back(pMove1);
    iMovesMatrix[eC][eA].push_back(pMove2);

    CHECK_THROW(iMovesMatrix.rewire(mvOutPendingMoves), std::exception);
}

TEST(rewire_badMatrix_inverse){ //something went wrong in fillMatrices() or just after...
    std::map<std::string, std::vector<MovementUnit>> mvOutPendingMoves;
    auto pAnimal1 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal2 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    auto pAnimal3 = std::make_shared<Animal>(nullptr, 20, 2, 0);
    mvOutPendingMoves["dest00"].emplace_back(MovementUnit("source00", eA, 66, pAnimal1, 0));
    mvOutPendingMoves["dest01"].emplace_back(MovementUnit("source01", eC, 66, pAnimal2, 0));
    mvOutPendingMoves["dest02"].emplace_back(MovementUnit("source02", eA, 66, pAnimal3, 0));
    auto pMove1 = std::make_shared<MovementForMatrix>("source00", "dest00", pAnimal1);
    auto pMove2 = std::make_shared<MovementForMatrix>("source01", "dest01", pAnimal2);
    auto pMove3 = std::make_shared<MovementForMatrix>("source00", "dest02", pAnimal3);
    MovesMatrix iMovesMatrix = MovesMatrix();
    iMovesMatrix[eA][eC].push_back(pMove1);
    iMovesMatrix[eC][eA].push_back(pMove2);
    iMovesMatrix[eA][eC].push_back(pMove3);

    CHECK_THROW(iMovesMatrix.rewire(mvOutPendingMoves), std::exception);
}
